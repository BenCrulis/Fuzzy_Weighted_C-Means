"""
Implements an intra-bundle diversity term.
This diversity is expressed
"""
from penalty.Penalty import Penalty
from utility.comparison import cos
import numpy as np

class DiversityTerm(Penalty):

    def __init__(self, sim):
        Penalty.__init__(self)
        self.sim = sim

    def evaluate(self, bundle):
        """
        Computes the intra-dissimilarity of of queries in one bundle
        :param bundle: set of queries as a Numpy ndarray
        :return: a real value representing the average dissimilarity between queries in the bundle
        """
        size = np.shape(bundle)[0]
        cpt = 0
        sum = 0
        for i in range(size):
            for j in range(i+1, size):
                sum += self.sim(bundle[i], bundle[j])
                cpt += 1
        return 1 - (sum / cpt)
