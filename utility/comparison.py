"""
This class contains shortcuts to usual distances, dissimilarities functions implemented with Numpy
between numpy ndarrays.
"""

import numpy as np

def cos(X, Y):
    return np.dot(X, Y) / pow(np.dot(X, X) * np.dot(Y, Y), 0.5)