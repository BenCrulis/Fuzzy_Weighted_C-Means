"""
Class FCMdd for Fuzzy C-Medoid clustering algorithm
Versions
0.1:Krista Drushku:03/30/2018:first complete version
"""

import numpy as np
import skfuzzy as fuzz
import matplotlib.pyplot as plt
import random as rd


bestFeaturesMatrix = np.genfromtxt("C:\\Users\\I325248\\Documents\\WeBI\\UC3\\testBestFeatures.csv", delimiter=';')

class FCMdd:

    def __init__(self, k, p, m, epsilon=10e-5, max_nb_iter=100, matrix=bestFeaturesMatrix):
        """
        Default constructor
        :param k: number of clusters
        :param p: number of medoids
        :param m: parameter of fuzzy clustering
        :param epsilon: convergence threshold
        :param max_nb_iter: maximal number of iterations
        """
        self.k = k
        self.p = p
        self.m = m
        self.membership = None
        self.data = None
        self.centers = None
        self.centerIDs = np.empty(k)
        self.newCenterIDs = np.empty(k)
        self.epsilon = epsilon
        self.max_iter = max_nb_iter
        self.J = None  # storing all the values of currentJ
        self.currentJ = float('inf')
        # imported matrix with all the distances between queries using feature-based metric
        self.matrix = matrix
        self.n = self.matrix.shape[0]
    # end __init__

    def randomInitCenters(self):
        index = np.random.randint(0,self.n, self.k)
        self.centers = self.matrix[index]
        self.centerIDs = index

    def randomInitMembership(self):

        self.membership = np.random.random((self.k,self.n))
        sum = np.sum(self.membership,axis=0)

        # alternate programming
        for i in range(self.n):
            if sum[i] != 0:
                self.membership[:,i] = (1.0 / sum[i]) * self.membership[:,i]
    # end randomInitMembership

    def innerTerm(self, i, j):
        """
        Compute the 'raw' membership between object i and cluster j
        Hypothesis: distance matrix is up to date
        :param i: index of object
        :param j: index of cluster
        :return: 'raw' membership (before handling problems like membership is infinite)
        """
        sub = 1 / (self.m - 1)
        sum = 0
        for k in range(self.k):
            #if self.matrix[self.centerIDs[k]][i] == 0: #if the distance between the point and the cluster is 0
            if self.centers[k][i] == 0 :
                return float('inf')
            else:
                t = 1 / self.centers[k][i]
                t = pow(t, sub)
                sum += t
        if sum == 0 or np.math.isnan(sum):
            return float('inf')
        else:
            return pow((1/self.centers[j][i]), sub) / sum

    def updateCenters(self):
        """
        This function updates clusters' centers based on matrices products provided by numpy
        :return: None
        """

        self.newCenterIDs = np.empty(self.k)
        for i in range(self.k):
            medoids = np.argpartition(self.membership[i], -self.p)[-self.p:] #p most closest points to center i
            pos = 0
            maxVal = float("inf")
            for j in medoids:
                sumDist = np.sum(np.matmul(pow(self.membership[i], self.m), self.matrix[j]))
                if j not in self.newCenterIDs:       #not to choose the same centers for different clusters
                    if sumDist < maxVal:
                        maxVal = sumDist
                        pos = j
            self.centers[i] = self.matrix[pos]
            self.centerIDs[i] = pos

    # end updateCenters

    def updateMembership(self):
        """
        This function updates clusters membership based on current centers coordinates
        The objective function value is updated as the sum of squared precalculated
        distances using the features for each point weighted by the membership of each
        point to each cluster
        :return: None
        """
        self.currentJ = 0

        # update distance matrix between points and clusters
        #distances = [[self.distance(self.matrix[i], self.centers[j]) for i in range(self.n)] for j in range(self.k)]

        # init membership matrix
        self.membership = np.zeros((self.k, self.n))

        for i in range(self.n):
            warning = -1
            for j in range(self.k):
                self.membership[j][i] = self.innerTerm(i, j)
                if self.membership[j][i] == float('inf'):
                    warning = j
                    break

            # if warning == -1:           # if no infinite membership
            #     sum = np.sum(self.membership[:,i], axis=0)              # TODO: to be checked!
            #     if abs(sum - 1) > 10e-6:
            #         print("Normalisation requise")
            #     # normalisation round 2
            #     for j in range(self.k):
            #         self.membership[j][i] = self.membership[j][i] / sum
            # else:
            if warning >= 0:
                self.membership[:, i] = np.zeros(self.k)
                self.membership[warning, i] = 1

        # computing the J value based on objective function Equation
        J = 0
        for j in range(self.k):
            self.currentJ += np.dot(np.power(self.membership[j],self.m), self.matrix[j])

    # end updateMembership()


    def fit(self):
        """
        Build the k clusters following traditional fc-means approach.
        :param dataset: a nxm list of lists or ndarray
        :return: updates the fuzzy membership matrix that indicates for each point
                    to which extent it belongs to the clusters
        """

        self.randomInitCenters()                  # causes some problems when computing innerTerm because of div by 0

        nbiter = 0
        previousJ = float('inf')
        prepreviousJ = float('inf')
        self.updateMembership()  # update membership and J value based on initial centers

        while nbiter <= self.max_iter:
            self.J = np.append(self.J, self.currentJ)
            nbiter += 1                     # one more iteration
            self.updateCenters()            # update centers coordinates
            self.updateMembership()         # update membership and J value
            if self.currentJ in self.J:
                break
    # end fit()

    def getJ(self):
        """
        This function returns the value of the objective function on this run
        :return: J value
        """
        return self.J

    def getClusterCenters(self):
        """
        This function returns the coordinates of the cluster centers as a (k clusters x m features) numpy ndarray
        :return:
        """
        return self.centers

    def getClusters(self):
        """
        Returns a 1D numpy array that indicates for each point the index of its cluster between 0 and k-1 included
        :return: 1D numpy array
        """
        res = np.zeros(self.n)
        for i in range(self.n):
            res[i] = np.argmax(self.membership[:,i])
        return res
    # end getClusters

    def clusterOverleap(self):
        """
        :return: the silhouette score of the clusters obtained
        """
        comp = 0
        overlapAVG = 0
        for i in range(self.k):
            for j in range(self.k-i-1):
                j=j+i+1
                overlapAVG += (np.sum(np.minimum(self.membership[i], self.membership[j])) / np.sum(np.maximum(self.membership[i], self.membership[j])))
                comp = comp+1
        return overlapAVG/comp
    #end clusterOverleap

    def getS(self):
        """
        Calculates the coefficient S of the compactness and separation
        intra and iter clusters.
        :return: S, the coefficient
        """

        S = 0
        dmin = float('inf')
        for j in range(self.k-1):
            S += np.matmul(pow(self.membership[j], self.m), pow(self.centers[j], 2))
            for i in range(self.k-1-j):
                i = j+1+i
                distCenters = self.centers[j][self.centerIDs[i]]
                if  distCenters < dmin:
                     dmin = distCenters

        if dmin == 0:
            S = 1
        else :
            S = S / (pow(dmin, 2) * self.n)
        return S

    #end getS


    def evaluateFCMdd(self, newk, newp, newm):
        """
        :param newk: array of clusters' size
        :param newp: array of different p size
        :param newm: array of different m parameters
        :return: the parameters that gives the small S coefficient
        """
        minS = float('inf')
        mink = float('inf')
        minp = float('inf')
        minm = float('inf')
        for i in newk:
            self.k = i
            for j in newp:
                self.p = j
                for l in newm:
                    self.m = l
                    self.fit()
                    S = self.getS()
                    if S < minS:
                        minS = S
                        mink = i
                        minp = j
                        minm = l

        print("Cluster size:")
        print(mink)
        print("p:")
        print(minp)
        print("m")
        print(minm)

        self.fit()



    #end evaluateFCMdd