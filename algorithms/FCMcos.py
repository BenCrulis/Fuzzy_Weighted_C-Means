"""
Class FCM for Fuzzy C-Means clustering algorithm
Versions
0.1:Nicolas Labroche:03/23/2018:first complete version
"""

from algorithms.KM import KM
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.preprocessing import normalize
import random as rd

class FCMcos(KM):

    def __init__(self, c, m = 0.8, epsilon = 1e-5, max_nb_iter = 100):
        """
        Default constructor
        :param c: number of clusters
        :param m: fuzzy exponent
        :param epsilon: convergence threshold
        :param max_nb_iter: maximal number of iterations
        """
        KM.__init__(self, c, epsilon, max_nb_iter)
        self.m = m
        # all other parameters are inherited from K-Means
    # end __init__

    def randomInitCenters(self):
        c = []
        while len(c) < self.k:
            index = rd.randint(0,self.n)
            while index in c:
                index = rd.randint(0, self.n)
            c.append(index)
        # index = np.random.randint(0,self.n, self.k)
        self.centers = self.data[c]

    def randomInitMembership(self):

        self.membership = np.random.random((self.k,self.n))
        sum = np.sum(self.membership,axis=0)

        # alternate programming
        for i in range(self.n):
            if sum[i] != 0:
                self.membership[:,i] = (1.0 / sum[i]) * self.membership[:,i]
    # end randomInitMembership

    # def updateSimilarities(self):
    #     self.sim = cosine_similarity(self.centers, self.data)


    def innerTerm(self, i, j):
        """
        Compute the 'raw' membership between object i and cluster j
        Hypothesis: similarity matrix is up to date
        :param i: index of object
        :param j: index of cluster
        :param sim: k*n similarity matrix
        :return: 'raw' membership (before handling problems like membership is infinite)
        """
        p = 1 / (1 - self.m)
        numerator = self.sim[j][i]              # np.dot(self.data[i], self.centers[j])
        sum = 0
        for c in range(self.k):
            sum += self.sim[c][i]

        return pow(numerator / sum, p)

    def updateMembership(self):
        self.membership = np.zeros((self.k, self.n))
        for i in range(self.n):
            sum = 0
            for j in range(self.k):
                self.membership[j][i] = self.innerTerm(i,j)
                sum += self.membership[j][i]
            for j in range(self.k):
                self.membership[j][i] = self.membership[j][i] / sum


    def updateMembership_old(self):
        """
        This function updates clusters membership based on current centers coordinates
        The objective function value is updated as the sum of cosine similarities
        :return: None
        """

        # init membership matrix
        self.membership = np.zeros((self.k, self.n))

        for i in range(self.n):
            warning = -1
            sumOverK = 0
            for j in range(self.k):
                self.membership[j][i] = self.innerTerm(i,j)
                if self.membership[j][i] == float('inf'):
                    # if innerTerm produces infinity value this means that cosine similarity is 0
                    warning = j
                    # break

            if warning >= 0:
                # TODO: check whenever possible if the following code works.
                # TODO: Needs some orthogonal datas where cosine is 0
                self.membership[warning, i] = 0
                sum = np.sum(self.membership[:,i])
                self.membership[:,i] = self.membership[:,i] * (1 / sum)
            #     self.membership[:, i] = np.zeros(self.k)
            #     self.membership[warning, i] = 42
    # end updateMembership()

    def updateObjective(self):
        # computing the J value based on objective function Equation
        self.J = 0
        for j in range(self.k):
            self.J += np.dot(np.power(self.membership[j], self.m), self.sim[j])

    def updateCenters(self):
        """
        Update centers coordinates based on Equation provided in TKDE 2017
        by Alsayanesh et al.
        :return: None
        """
        for j in range(self.k):
            Aj = np.zeros((self.natt))

            for r in range(self.natt):
                sum = 0
                for i in range(self.n):
                    sum += pow(self.membership[j][i], self.m) * self.data[i][r]
                Aj[r] = sum / self.n

            # compute L2 norm of Aj
            norm = np.linalg.norm(Aj)

            # compute centers coordinates
            self.centers[j] = Aj / norm

        #self.centers = np.matmul(np.power(self.membership,self.m),self.data)
        #self.centers = self.centers * (1 / self.n)

        #A2jr = np.power(self.centers, 2)        # normalization
        #A2jr = np.sum(A2jr, axis=1)             # sum over the features dimension 'r'
        #A2jr = np.power(A2jr, 0.5)              # square root

        # normalizing centers: computing average
        #for j in range(self.k):
        #    self.centers[j] = self.centers[j] * (1 / A2jr[j])

    def normalizeToUnitLength(self):
        """
        This function normalizes to unit length each data object to make it
        compatible with cosine similarity.
        :return: nothing, modifies the dataset
        """
        for i in range(self.n):
            norm = np.linalg.norm(self.data[i])
            self.data[i] = self.data[i] * (1 / norm)

    def updateSimilarities(self):
        self.sim = np.zeros((self.k, self.n))
        for j in range(self.k):
            for i in range(self.n):
                self.sim[j][i] = np.dot(self.data[i], self.centers[j])

    def fit(self, dataset):
        """
        Build the k clusters following traditional fc-means approach
        But with as cosine similarity
        :param dataset: a nxm list of lists or ndarray
        :return: updates the fuzzy membership matrix that indicates for each point
                    to which extent it belongs to the clusters
        """

        self.data = normalize(np.asarray(dataset))
        self.n = self.data.shape[0]
        self.natt = self.data.shape[1]

        self.sim = np.zeros((self.k, self.n))

        # Compute initial centers and distance matrix
        self.randomInitCenters()
        self.updateSimilarities()
        self.updateMembership()
        previousJ = float('inf')
        self.updateObjective()

        nbiter = 0

        # self.updateMembership()  # update membership and J value based on initial centers

        while nbiter <= self.max_iter and abs(previousJ - self.J) > self.epsilon:
            nbiter += 1                     # one more iteration
            self.updateCenters()            # update centers coordinates
            self.updateSimilarities()       # update similarity matrix
            self.updateMembership()         # update membership and J value
            previousJ = self.J              # storing previous J value
            self.updateObjective()

    # end fit()

    def getJ(self):
        """
        This function returns the value of the objective function on this run
        :return: J value
        """
        return self.J

    def getClusterCenters(self):
        """
        This function returns the coordinates of the cluster centers as a (k clusters x m features) numpy ndarray
        :return:
        """
        return self.centers

    def getClusters(self):
        """
        Returns a 1D numpy array that indicates for each point the index of its cluster between 0 and k-1 included
        :return: 1D numpy array
        """
        res = np.zeros(self.n)
        for i in range(self.n):
            res[i] = np.argmax(self.membership[:,i])
        return res