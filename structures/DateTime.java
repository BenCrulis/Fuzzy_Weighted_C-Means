package dataStructures;

import java.util.HashMap;

public class DateTime {
	
	public int month;
	public int date;
	public int year;
	public int time;
	public int ampm;
	public HashMap<String, Integer> months = new HashMap<String, Integer>(){
		{ 
			put("Jan", 1); 
			put("Feb", 2); 
			put("Mar", 3); 
			put("Apr", 4);
			put("May", 5);
			put("Jun", 6); 
			put("Jul", 7); 
			put("Aug", 8); 
			put("Sep", 9);
			put("Oct", 10);
			put("Nov", 11);
			put("Dec", 12);
		}
	};
	
	
	public DateTime() {
		this.month = 0;
		this.date = 0;
		this.year = 0;
		this.time = 0;
		this.ampm = 0;
	}
	
	public DateTime(String month, int date, int year, String hour) {
		this.month = months.get(month);
		this.date = date;
		this.year = year;
		this.time = Integer.parseInt(hour.substring(0, hour.indexOf(":")).concat(hour.substring(hour.indexOf(":")+1, hour.indexOf(":")+3)));
		String ampmS = hour.substring(hour.indexOf(":")+3, hour.length());
		this.ampm = ampmS.startsWith("A") ? 0 : 1;  
	}
	
	
	
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getDate() {
		return date;
	}
	public void setDate(int date) {
		this.date = date;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	public int getAmpm() {
		return ampm;
	}
	public void setAmpm(int ampm) {
		this.ampm = ampm;
	}
	
	public static int getDiffTime (DateTime t1, DateTime t2 )
	{	
		if(t1.date==t2.date && t1.ampm==t2.ampm)
			return Math.abs(t1.time-t2.time);
		else if (t1.date==t2.date && t1.ampm!=t2.ampm )
			return (1200 + Math.abs(t1.time-t2.time));
		else if (t1.date<t2.date)
		{
			if(t1.ampm==1 && t2.ampm==0)
			return (1200 - Math.abs(t1.time-t2.time));
		}
		return 1201;
		
			
	}
	
	public static int getDiffDate (DateTime t1, DateTime t2)
	{	
		if(t1.month==t2.month)
			return Math.abs(t1.date-t2.date);
		else
			return (30 - Math.abs(t1.date-t2.date));
	}
	
	public static int getDiffMonth (DateTime t1, DateTime t2)
	{	
		if(t1.year==t2.year)
			return Math.abs(t1.month-t2.month);
		else 
			return (12 - Math.abs(t1.month-t2.month));
	}

}
