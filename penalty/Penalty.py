"""
Abstract class PenaltyTerm to represent a penalty term in the bundle construction
Versions
0.1:Nicolas Labroche:03/22/2018:first attempt
0.2:Nicolas Labroche:04/06/2018:generalization of penalty term model, introduction of weight attribute
"""

class Penalty:

    def __init__(self):
        """
        This class represents a penalty term in an objective function.
        A penalty term has a weight and can be evaluated
        """
        pass

    def evaluate(self):
        """
        Abstract method to evaluate a penalty term for a bundle/report/user profile made of several queries
        """
        raise NotImplementedError("Should have implemented this")

    def evaluateSingle(self):
        """
        Abstract method to evaluate a penalty term for a single query
        """
        raise NotImplementedError("Should have implemented this")

# end class Penalty