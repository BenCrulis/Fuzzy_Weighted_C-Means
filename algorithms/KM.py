"""
Class KM for K-Means clustering algorithm
Versions
0.1:Nicolas Labroche:03/22/2018:first complete version
0.2:Nicolas Labroche:03/23/2018:now manages random membership assignment when several clusters are equally distant
"""

import numpy as np
import random as rd


class KM:

    def __init__(self, k, epsilon = 10e-5, max_nb_iter = 100):
        """
        Default constructor
        :param k: number of clusters
        :param epsilon: convergence threshold
        :param max_nb_iter: maximal number of iterations
        """
        self.k = k
        self.membership = None
        self.data = None
        self.centers = None
        self.epsilon = epsilon
        self.max_iter = max_nb_iter
        self.J = float('inf')               # initial value of objective function to minimize

    def distance(self, X, Y):
        """
        Squared Euclidian distance
        :param X: first vector
        :param Y: second vector
        :return: a real value representing the Euclidian distance between vector X and Y
        """
        tmp = np.subtract(X, Y)
        return np.dot(tmp, tmp)

    def randomInitCenters(self):
        index = np.random.randint(0,self.n, self.k)
        self.centers = self.data[index]

    def randomInitMembership(self):

        self.membership = np.zeros((self.k,self.n)) # warning for computation efficiency, the matrix is transposed

        for i in range(self.n):
            self.membership[rd.randint(0,self.k-1)][i] = 1

    def updateCenters(self):
        """
        This function updates clusters' centers based on matrices products provided by numpy
        :return: None
        """
        # self.centers = np.zeros((self.k, self.natt))
        # the next instruction can be optimized by storing a transverse matrix for membership
        self.centers = np.matmul(self.membership,self.data)
        # number of elements in each cluster, ie. lines of membership matrix (axis=1)
        weights = np.sum(self.membership,axis=1)

        # alternate programming
        for j in range(self.k):
            if weights[j] != 0:
                self.centers[j] = (1 / weights[j]) * self.centers[j]

        # normalizing centers: computing average
        # tmp = np.transpose(self.centers)
        # tmp = np.divide(tmp, weights)           # TODO: Error might happen when a weight is equal to 0
                                                # TODO: This should be corrected
        # self.centers = np.transpose(tmp)

    def updateMembership(self):
        """
        This function updates clusters membership based on current centers coordinates
        The objective function value is updated as the sum of minimal squared Euclidian
        distance assignment for each point
        :return: None
        """
        self.J = 0
        self.membership = np.zeros((self.k, self.n))
        for i in range(self.n):
            min = 0
            indmin = 0
            indmin2 = list()    # list of closest clusters index in case they are not unique
            for j in range(self.k):
                # compute distance between xi and cj
                d = self.distance(self.data[i],self.centers[j])
                if j == 0:
                    min = d
                    indmin = 0
                    indmin2.append(0)
                else:   # j != 0
                    if d == min:
                        indmin2.append(j)   # in case there is equality with the current best, we keep them all
                    elif d < min:
                        indmin2.clear()     # in case we find a better solution, we remove all current best solutions
                        min = d
                        indmin = j
                        indmin2.append(j)

            self.J += min
            # old solution where only the first closest cluster receive the point
            # self.membership[indmin][i] = 1

            # new solution to handle a point that could be assigned to several clusters that are equally distant
            if len(indmin2) > 1:
                ind = rd.randint(0,len(indmin2)-1)
                self.membership[indmin2[ind]][i] = 1
            else:
                self.membership[indmin][i] = 1      # idem old solution

        # check if one or several clusters are empty
        points_moved = []
        tmp = np.sum(self.membership, axis=1)
        for j in range(self.k):
            if tmp[j] == 0:
                # select a point at random by ensuring that it has not been used previously to solve the problem
                # of empty cluster for an other cluster
                goOn = True    # True while the chosen point has been used to solve the emptiness problem
                                    # of a previous cluster
                item = 0            # index of data object to change cluster
                while (goOn):
                    item = rd.randint(0,self.n)
                    goOn = item in points_moved or np.max(self.membership[:,item]) == 0
                points_moved.append(item)
                # find its cluster
                its_cluster = np.argmax(self.membership[:,item])
                # and assign it to j instead
                self.membership[its_cluster,item] = 0
                self.membership[j,item] = 1
    # end updateMembership()


    def fit(self, dataset):
        """
        Build the k clusters following traditional k-means approach.
        :param dataset: a nxm list of lists or ndarray
        :return: updates the binary membership matrix that indicates for each point the cluster it belongs to
        """

        self.data = np.asarray(dataset)
        self.n = self.data.shape[0]
        self.natt = self.data.shape[1]

        # Initialize random membership to clusters
        #self.randomInitMembership()

        # Compute initial centers
        #self.updateCenters()
        self.randomInitCenters()

        nbiter = 0
        previousJ = float('inf')
        self.updateMembership()  # update membership and J value based on initial centers

        while nbiter <= self.max_iter and abs(previousJ - self.J) > self.epsilon:
            nbiter += 1                     # one more iteration
            self.updateCenters()            # update centers coordinates
            previousJ = self.J              # storing previous J value
            self.updateMembership()         # update membership and J value

    # end fit()

    def getJ(self):
        """
        This function returns the value of the objective function on this run
        :return: J value
        """
        return self.J

    def getClusterCenters(self):
        """
        This function returns the coordinates of the cluster centers as a (k clusters x m features) numpy ndarray
        :return:
        """
        return self.centers

    def getClusters(self):
        """
        Returns a 1D numpy array that indicates for each point the index of its cluster between 0 and k-1 included
        :return: 1D numpy array
        """
        res = np.zeros(self.n)
        for i in range(self.n):
            res[i] = np.argmax(self.membership[:,i])
        return res