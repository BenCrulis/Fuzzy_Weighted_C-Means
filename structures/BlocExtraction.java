package calculations;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import dataStructures.Document;
import dataStructures.Filter;
import dataStructures.HierarchicalLevel;
import dataStructures.WebIBlock;

public class BlocExtraction {

	
	public static void main(String[]args) throws SQLException, FileNotFoundException, IOException
	{
	//	long beg=System.currentTimeMillis();
		
		String connection = "jdbc:postgresql://[::1]:5432/audit1503";
		String user = "postgres";
		String pass = "Password1";
        Connection con = DriverManager.getConnection(connection, user, pass);

        String varProcedure="matrix"; // "weight" to calculate the weights of the model and "matrix" to calculate the final matrix of dissimilarity
        String blocbase="blocklist"; 
     //   String auditbase="audityassine"; 
        String auditselected = "recentcreated";
        	
        BlocFeatureBasedComparison bfbc = new BlocFeatureBasedComparison();
        
        if(varProcedure.equals("weight"))
        {
        	ArrayList<WebIBlock> allBlocs =  extractBlocs(con,blocbase, auditselected);
            WebIBlock[] allB = new WebIBlock[allBlocs.size()];
            int i=0;
            for(WebIBlock ev:allBlocs)
            {
            	allB[i]= ev;
            	i++;
            }
          
       
        float matrix [][]= bfbc.calculMatriceBlocs(allB, auditselected, blocbase, con);
        bfbc.writeCsvFile("C:\\Users\\I325248\\workspaceLuna\\Weights\\blocMatrix.csv", matrix );
       
        }
        else
        {
        	ArrayList<WebIBlock> allBlocs =  extractBlocs(con,blocbase, auditselected);
           
 	       // dissimilarityMatrix ("C:/Users/I325248/Documents/TOURS/WEBI/Real User Tests Protocol/recentCreated/G6.csv",  documents, positions, allDocs, null, maxRank, npath, usergroups, titles);  // matrix of similarity
        	HashMap<String,ArrayList<DocUsage>> usage = bfbc.getTotalUsersOfDocument( auditselected,blocbase,con, "userid") ;
        	HashMap<String,ArrayList<DocUsage>> folder = bfbc.getTotalUsersOfDocument( auditselected,blocbase,con, "folderid") ;
	        dissimilarityMatrix ("C:/Users/I325248/Documents/TOURS/WEBI/UC3 Blocs/testBestFeatures.csv",  allBlocs, auditselected,blocbase, con, usage, folder);
    
	  //    similarityBetweenReports("C:/Users/I325248/Documents/TOURS/WEBI/UC3 Blocs/testBestFeatures.csv",  allBlocs, auditselected,blocbase, con, usage, folder);
	      
	      
        }

   //    System.out.print("Time: "+(System.currentTimeMillis()-beg));
	        
}
	

	private static ArrayList<WebIBlock> extractBlocs(Connection conHana, String blockbase, String auditbase) throws SQLException {
	
		ArrayList<WebIBlock> allBlocs = new ArrayList<WebIBlock>();
		
		Statement smt = conHana.createStatement();
		String sql = "Select * from "+blockbase+"  where parent in (select distinct parent from "+auditbase+")order by parent, reportid, blockid";
		ResultSet rs = smt.executeQuery(sql);
		
		String doc="", report="", bloc="";
		
		WebIBlock newWB = new WebIBlock();
		while(rs.next())
		{
			
			
					if(doc.equals(rs.getString(1)) && report.equals(rs.getString(2)) && bloc.equals(rs.getString(3))) //the bloc has already starting construction
					{
						String qualif = rs.getString(10);
						if(qualif.equals("MEASURE"))
						{
							ArrayList<String> meas = newWB.getMeasures();
							meas.add(rs.getString(6));
							newWB.setMeasures(meas);
						}
						else if (qualif.equals("DIMENSION"))
						{
							ArrayList<String> dim = newWB.getDimensions();
							dim.add(rs.getString(6));
							newWB.setDimensions(dim);
						}
						else if (qualif.equals("FILTER"))
						{
							ArrayList<Filter> filters = newWB.getFilters();
							ArrayList<String> values = FeatureExtractionDocument.VectorToArray(rs.getString(7).split(","));
							Filter filter = new Filter(rs.getString(6), values);
							filters.add(filter);
							newWB.setFilters(filters);
						}
					}
					else
					{
						if(!doc.equals(""))
							allBlocs.add(newWB);
					// new bloc start. Update doc, report and bloc IDs
					
				doc = rs.getString(1);
				report = rs.getString(2);
				bloc = rs.getString(3);
				
				newWB = new WebIBlock();
				newWB.setIdDocument(doc);
				newWB.setIdReport(report);
				newWB.setIdBlock(bloc);
				newWB.setBlocktype(rs.getString(4));
				newWB.setBlockviz(rs.getString(5));
				newWB.setUniverse(rs.getString(8));
				
				String qualif = rs.getString(10);
				
				if(qualif.equals("MEASURE"))
				{
					ArrayList<String> meas = new ArrayList<String>();
					meas.add(rs.getString(6));
					newWB.setMeasures(meas);
				}
				else if (qualif.equals("DIMENSION"))
				{
					ArrayList<String> dim = new ArrayList<String>();
					dim.add(rs.getString(6));
					newWB.setMeasures(dim);
				}
				else if (qualif.equals("FILTER"))
				{
					ArrayList<Filter> filters = new ArrayList<Filter>();
					ArrayList<String> values = FeatureExtractionDocument.VectorToArray(rs.getString(7).split(","));
					Filter filter = new Filter(rs.getString(6), values);
					filters.add(filter);
					newWB.setFilters(filters);
				}
			}
			
		}
		
		allBlocs.add(newWB);
		
		return allBlocs;
	}
	



	private static void dissimilarityMatrix(String file, ArrayList<WebIBlock> allBlocs, String auditbase, String blocbase, Connection con, HashMap<String, ArrayList<DocUsage>> usage, HashMap<String, ArrayList<DocUsage>> folder) throws IOException, SQLException {
		FileWriter bw = new FileWriter(file);
		Double[][] matrix = new Double[allBlocs.size()][allBlocs.size()];
		double sim = 0;
		
		for(int i = 0; i<allBlocs.size()-1; i++)
		{
			
		//	bw.append(allBlocs.get(i).getDocID()+";");
			matrix[i][i] = (double) 0;
			for (int j=i+1; j<allBlocs.size(); j++)
			{	 
				
		        sim = BlocFeatureBasedComparison.compareBlocs (allBlocs.get(i), allBlocs.get(j), auditbase, blocbase, con, usage, folder);
				matrix[i][j] = sim;
				matrix[j][i] = sim;
			}
			
		} 
	//	bw.append(allBlocs.get(allBlocs.size()-1).getDocID());
			
		
	
		bw.append("\n");
		for(int i = 0; i<allBlocs.size(); i++)
		{
			for (int j=0; j<allBlocs.size(); j++)
			{	
				if(j!=allBlocs.size()-1)
					bw.append(String.valueOf(matrix[i][j])+";");
				else 
					bw.append(String.valueOf(matrix[i][j]));
			}
			if(i!=allBlocs.size()-1)
			bw.append("\n");
		} 
			
		bw.close();
		
	}
	
	
	private static void similarityBetweenReports(String file, ArrayList<WebIBlock> allBlocs, String auditbase, String blocbase, Connection con, HashMap<String, ArrayList<DocUsage>> usage, HashMap<String, ArrayList<DocUsage>> folder) throws IOException, SQLException {
		FileWriter bw = new FileWriter(file);
		Double[][] matrix = new Double[allBlocs.size()][allBlocs.size()];
		
		HashMap<String, ArrayList<Double>> repblocs = new HashMap<String, ArrayList<Double>>();
		double sim = 0;
		ArrayList<Double> temp=new ArrayList<Double>();
		
		for(int i = 0; i<allBlocs.size()-1; i++)
		{
			
			for (int j=i+1; j<allBlocs.size(); j++)
			{	 
				
				if(allBlocs.get(i).getIdDocument().equals(allBlocs.get(j).getIdDocument()) &&
						allBlocs.get(i).getIdReport().equals(allBlocs.get(j).getIdReport()))
				{
		       // sim = BlocFeatureBasedComparison.compareBlocs (allBlocs.get(i), allBlocs.get(j), auditbase, blocbase, con, usage, folder);
			      BlocFeatureBasedComparison bfbc = new BlocFeatureBasedComparison();
					sim = 0.5*bfbc.sameMeasOrDim(allBlocs.get(i), allBlocs.get(j), "Measure")+//+0*bfbc.sameMeasOrDim(allBlocs.get(i), allBlocs.get(j), "Dimension")+
			  				0.5*bfbc.sameFilter(allBlocs.get(i), allBlocs.get(j));
			      
		        if(repblocs.containsKey(allBlocs.get(i).getIdDocument()+"_"+allBlocs.get(i).getIdReport()))
		        {
		        	temp = repblocs.get(allBlocs.get(i).getIdDocument()+"_"+allBlocs.get(i).getIdReport());
		        	temp.add(sim);
		        	repblocs.replace(allBlocs.get(i).getIdDocument()+"_"+allBlocs.get(i).getIdReport(), temp);
		        }
		        else
		        {
		        	temp = new ArrayList<Double>();
		        	temp.add(sim);
		        	repblocs.put(allBlocs.get(i).getIdDocument()+"_"+allBlocs.get(i).getIdReport(), temp);
		        }
				
				}
				
			}

		}
		double sum = 0;
		Iterator it = repblocs.entrySet().iterator();
		while(it.hasNext())
		{
			Entry<String, ArrayList<Double>> pair = (Entry<String, ArrayList<Double>>) it.next();
			for(Double d: pair.getValue())
				sum+=(d/(double)pair.getValue().size())/repblocs.size();
		}
		
		System.out.println("Intra-similarity: "+sum);
	}

	
}


