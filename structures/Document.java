package dataStructures;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;


public class Document {
	
	private ArrayList<String> timestamp;
	private ArrayList<String> sessionID;
	private HashMap<String, Integer> userID;
	private ArrayList<String> eventID;
	private HashMap<String, Integer> eventType;
	private String docID;
	private ArrayList<String> docMeasures;
	private ArrayList<String> docDimensions;
	private HashMap<String, ArrayList<String>> filters;
	private ArrayList<String> universID;
	private ArrayList<String> folderID;
	private HashMap<String, Integer> promptsname;
	private HashMap<String, Integer> promptsvalue;
	private HashMap<String, Integer> viz;
	private ArrayList<Report> reports;
	
	public Document()
	{
		this.timestamp = new ArrayList<String>();
		this.sessionID = new ArrayList<String>();
		this.universID = new ArrayList<String>();
		this.userID = new HashMap<String, Integer>();
		this.eventID = new ArrayList<String>();
		this.eventType = new HashMap<String, Integer>();
		this.docID = "";
		this.docMeasures = new ArrayList<String>();
		this.docDimensions = new ArrayList<String>();
		this.filters = new HashMap<String, ArrayList<String>>();
		this.folderID = new ArrayList<String>();
		this.promptsname = new HashMap<String, Integer>();
		this.promptsvalue = new HashMap<String, Integer>();
		this.viz = new HashMap<String, Integer>();
		this.reports = new ArrayList<Report>();
	}
	
	
	//README: use this constructor to take the values from the databases and create an Event 
	public Document (ArrayList<String> time, ArrayList<String> session, HashMap<String, Integer> user, ArrayList<String> eventId, HashMap<String, Integer> eventType, String doc,ArrayList<String> folder, ArrayList<String> universeID, ArrayList<String> measures, 
			ArrayList<String> dimensions, HashMap<String, ArrayList<String>> filters, HashMap<String, Integer> promptname, HashMap<String, Integer> promptvalue,HashMap<String, Integer> viz, ArrayList<Report> reports) throws SQLException
	{
		
		this.timestamp = time;
		this.sessionID = session;
		this.folderID = folder;
		this.universID = universeID;
		this.userID = user;
		this.eventID = eventId;
		this.eventType = eventType;
		this.docID = doc;
		this.docMeasures = measures;
		this.docDimensions = dimensions;
		this.filters = filters;
		this.promptsname = promptname;
		this.promptsvalue = promptvalue;
		this.viz=viz;
		this.reports = reports;
		
	}



	
	
	public ArrayList<String> getTimestamp() {
		return timestamp;
	}


	public void setTimestamp(ArrayList<String> timestamp) {
		this.timestamp = timestamp;
	}


	public ArrayList<String> getSessionID() {
		return sessionID;
	}


	public void setSessionID(ArrayList<String> sessionID) {
		this.sessionID = sessionID;
	}


	public HashMap<String, Integer> getUserID() {
		return userID;
	}


	public void setUserID(HashMap<String, Integer> userID) {
		this.userID = userID;
	}


	public ArrayList<String> getEventID() {
		return eventID;
	}


	public void setEventID(ArrayList<String> eventID) {
		this.eventID = eventID;
	}


	public HashMap<String, Integer> getEventType() {
		return eventType;
	}


	public void setEventType(HashMap<String, Integer> eventType) {
		this.eventType = eventType;
	}


	public String getDocID() {
		return docID;
	}


	public void setDocID(String docID) {
		this.docID = docID;
	}


	public ArrayList<String> getDocMeasures() {
		return docMeasures;
	}


	public void setDocMeasures(ArrayList<String> docMeasures) {
		this.docMeasures = docMeasures;
	}


	public ArrayList<String> getDocDimensions() {
		return docDimensions;
	}


	public void setDocDimensions(ArrayList<String> docDimensions) {
		this.docDimensions = docDimensions;
	}


	public HashMap<String, ArrayList<String>> getFilters() {
		return filters;
	}


	public void setFilters(HashMap<String, ArrayList<String>> filters) {
		this.filters = filters;
	}


	public ArrayList<String> getUniversID() {
		return universID;
	}


	public void setUniversID(ArrayList<String> universID) {
		this.universID = universID;
	}


	public ArrayList<String> getFolderID() {
		return folderID;
	}


	public void setFolderID(ArrayList<String> folderID) {
		this.folderID = folderID;
	}


	

	public HashMap<String, Integer> getPromptsname() {
		return promptsname;
	}


	public void setPromptsname(HashMap<String, Integer> promptsname) {
		this.promptsname = promptsname;
	}


	public HashMap<String, Integer> getPromptsvalue() {
		return promptsvalue;
	}


	public void setPromptsvalue(HashMap<String, Integer> promptsvalue) {
		this.promptsvalue = promptsvalue;
	}


	public HashMap<String, Integer> getViz() {
		return viz;
	}


	public void setViz(HashMap<String, Integer> viz) {
		this.viz = viz;
	}


	public ArrayList<Report> getReports() {
		return reports;
	}


	public void setReports(ArrayList<Report> reports) {
		this.reports = reports;
	}


public static Document createDocByID (Connection conHana, String idD) throws SQLException  {

		
		Statement smt = conHana.createStatement();
		ResultSet rs = null;

			
			String q = "Select distinct datetime, sessionid, userid, eventid, eventtype, folderid, universecuid from auditFinalLogs where parent='"+idD+"' order by eventid";
			rs = smt.executeQuery(q);
	
			ArrayList<String> timestamps = new ArrayList<String>();
			ArrayList<String> sessions = new ArrayList<String>();
			HashMap<String, Integer> userid = new HashMap<String, Integer>();
			ArrayList<String> eventid = new ArrayList<String>();
			HashMap<String, Integer> eventtype = new HashMap<String, Integer>();
			ArrayList<String> folderid = new ArrayList<String>();
			ArrayList<String> universe = new ArrayList<String>();
			while(rs.next())
			{
				if(rs.getString(1)!=null && !timestamps.contains(rs.getString(1)))
					timestamps.add(rs.getString(1));
				
				if(rs.getString(2)!=null && !sessions.contains(rs.getString(2)))
				    sessions.add(rs.getString(2));
				
				if(rs.getString(3)!=null)
				if(userid.containsKey(rs.getString(3)))
				{
					int freq = userid.get(rs.getString(3));
					userid.replace(rs.getString(3), freq+1);
				}
				else
					userid.put(rs.getString(3), 1);
				
				if(rs.getString(4)!=null && !eventid.contains(rs.getString(4)))
				eventid.add(rs.getString(4));
				
				if(rs.getString(5)!=null)
				if(eventtype.containsKey(rs.getString(5)))
				{
					int freq = eventtype.get(rs.getString(5));
					eventtype.replace(rs.getString(5), freq+1);
				}
				else
					eventtype.put(rs.getString(5), 1);
				
				if(rs.getString(6)!=null && !folderid.contains(rs.getString(6)))
				folderid.add(rs.getString(6));
				
				if(rs.getString(7)!=null && !universe.contains(rs.getString(7)))
				universe.add(rs.getString(7));

			}
			
			
			q = "SELECT detailname FROM docdetails WHERE detailtype=3 AND objectid='"+idD+"'";
			rs = smt.executeQuery(q);
			ArrayList<String> measures = new ArrayList<String>();
			while(rs.next())
			{
				measures.add(rs.getString(1));
			}
			q = "SELECT DISTINCT detailname FROM docdetails WHERE detailtype=2 AND objectid='"+idD+"'";
			rs = smt.executeQuery(q);
			ArrayList<String> dimensions = new ArrayList<String>();
			while(rs.next())
			{
				dimensions.add(rs.getString(1));
			}
			q= "SELECT DISTINCT detailname, detailvalue FROM docdetails WHERE detailtype=1 AND objectid='"+idD+"'";
			rs = smt.executeQuery(q);
			HashMap<String, ArrayList<String>> fil = new HashMap<String, ArrayList<String>>();
			while(rs.next())
			{
				if(fil.containsKey(rs.getString(1)))
				{
					ArrayList<String> objects = fil.get(rs.getString(1));
					objects.add(rs.getString(2));
					fil.replace(rs.getString(1), objects);
				}
				else
				{
					ArrayList<String> object = new ArrayList<String>();
					object.add(rs.getString(2));
					fil.put(rs.getString(1),object );
				}
			}
			
			q= "SELECT distinct eventdetailvalue FROM auditFinalLogs WHERE parent='"+idD+"' and eventdetailtype='Prompt Name'";
			rs = smt.executeQuery(q);
			HashMap<String, Integer> promptnames = new HashMap<String, Integer>();
			while(rs.next())
			{
				if(promptnames.containsKey(rs.getString(1)))
				{
					int freq = promptnames.get(rs.getString(1));
					promptnames.replace(rs.getString(1), freq+1);
				}
				else
					promptnames.put(rs.getString(1), 1);
			}
			q= "SELECT distinct eventdetailvalue FROM auditFinalLogs WHERE parent='"+idD+"' and eventdetailtype='Prompt Value'";
			rs = smt.executeQuery(q);
			HashMap<String, Integer> promptvalues = new HashMap<String, Integer>();
			while(rs.next())
			{
				if(promptvalues.containsKey(rs.getString(1)))
				{
					int freq = promptvalues.get(rs.getString(1));
					promptvalues.replace(rs.getString(1), freq+1);
				}
				else
					promptvalues.put(rs.getString(1), 1);
			}
			
			
			HashMap<String, Integer> viz1= new HashMap<String, Integer>();
			String query = "Select distinct reportid, blockid, blockviz from blocklist where parent='"+idD+"' order by reportid";
			rs = smt.executeQuery(query);
			while(rs.next())
			{
				if(viz1.containsKey(rs.getString(3)))
				{
					int value = viz1.get(rs.getString(3));
					value++;
					viz1.replace(rs.getString(3), value);
				}
				else
					viz1.put(rs.getString(3), 1);
			}
			rs.close();
			
			ArrayList<Report> reports = new ArrayList<Report>();
			query = "Select * from blocklist where parent='"+idD+"' order by reportid";
			rs = smt.executeQuery(query);
			String rep="";
			String block="";
			WebIBlock wb = null ;
			ArrayList meas, dim;
			ArrayList<WebIBlock> blocks = new ArrayList<WebIBlock>();
			while(rs.next())
			{
				if(rep.equals(rs.getString(2)))
				{
				
				if(block.equals(rs.getString(3)))
				{
				
					if(rs.getString(10)=="DIMENSION")
					{
						dim = wb.getDimensions();
						dim.add(rs.getString(6));
						wb.setDimensions(dim);
					}
					else
					{
						meas = wb.getMeasures();
						meas.add(rs.getString(6));
						wb.setMeasures(meas);
					}
				
				}
				else
				{
					if(block!="")
						blocks.add(wb);
					
					else
					{
					
					block = rs.getString(3);
					
					wb = new WebIBlock();
					wb.setIdDocument(idD);
					wb.setIdReport(rep);
					wb.setIdBlock(block);
					wb.setBlocktype(rs.getString(4));
					wb.setBlockviz(rs.getString(5));
					meas = new ArrayList<String>();
					dim = new ArrayList<String>();
					if(rs.getString(10)=="DIMENSION")
						dim.add(rs.getString(6));
					else
						meas.add(rs.getString(6));
					wb.setDimensions(dim);
					wb.setMeasures(meas);
					}
				}
				}
				else
				{
					if(rep!="")
						reports.add(new Report(idD, rep, blocks));
				}
				
			}
			
			Document doc = new Document(timestamps, sessions, userid, eventid, eventtype, idD, folderid,universe,  measures, dimensions, fil, promptnames, promptvalues, viz1, reports);
			
		return doc;
	}
	
	
	@Override
	public String toString() {
		return "Event [timestamp=" + this.getTimestamp() + ", session="
				+ this.getSessionID() + ", user=" + this.getUserID() + ", eventType=" + this.eventType + ", folder=" + this.folderID+  ", documentID=" + this.getDocID() + ", universes="
				+ this.getUniversID().toString() + ", measures=" + this.getDocMeasures() + ", dimensions=" + this.getDocDimensions() + ", filters=" + this.getFilters() + ", promptsnames=" + this.getPromptsname() + ", promptsvalues=" + this.getPromptsvalue()+"]";
	}

}
