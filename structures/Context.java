package dataStructures;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


public class Context {
	
	protected ArrayList<Event> events;
	//protected static HashSet<AlignmentIndexes> alignmentsIndexes;
	
	
	public Context()
	{
		this.events = new ArrayList<Event>();
	}
	
	public Context(ArrayList<Event> arrayList)
	{
		this.events = arrayList;
	}

	public ArrayList<Event> getevents() {
		return events;
	}
	
	public void addEvent(Event tmp)
	{
		this.events.add(tmp);
	}
   
//	public HashSet<AlignmentIndexes> getAlignmentsIndexes() {
//		return alignmentsIndexes;
//	}
//
//	public void setAlignmentsIndexes(HashSet<AlignmentIndexes> alignmentsIndexes) {
//		this.alignmentsIndexes = alignmentsIndexes;
//	}
//
//	
//	public static void addAlignmentIndexes(AlignmentIndexes alignmentIndexes){
//        alignmentsIndexes.add(alignmentIndexes);
//    }
	
//	public static void saveEvent(String q, List<String> s, QuerySuggestionImpl ch, String id, String user, Integer analyseId, DataSourceInfos ds){
//		List<Match> queryParts = ch.getMatches();
//		Iterator<Match> it = queryParts.iterator();
//		List<String> dimensions = new ArrayList<String>();
//		List<String> filters = new ArrayList<String>();
//		List<String> measures = new ArrayList<String>();
//		
//		for (Axis a : ch.getQueryStructure().getAxes()){
//			if (a instanceof NumericalAxis){
//				for (Component c : a.getComponents()){
//					measures.add(c.getAttribute().toString());
//				}
//			}
//			else if (a instanceof CategoricalAxis)
//			{
//				for (Component c : a.getComponents()){
//					dimensions.add(c.getAttribute().toString());
//				}
//			}
//		}
//		for (Filter f : ch.getFilters()){
//			if (!(f instanceof FormulaFilter))
//				filters.add(f.toString());
//		}
//		
//		
//		
////		while(it.hasNext())
////		{
////			Match match = it.next();
////			MatchType type = match.getType();
////			switch (type){
////				case MEASURE :
////					measures.add(match.toString());
////				break;
////				case ATTRIBUTE : dimensions.add(match.toString());
////				break;
////				case ATTRIBUTE_INSTANCE : filters.add(match.toString());
////				break;
////				default: break;
////		}
////		}
//		
//		String values = "";
//		
//		values += "'"+id + "','"+user+"','"+q+"','"+s.toString()+"','";
//		/*List<Match> queryParts = obs.getChosen().getMatches();
//		Iterator<Match> it = queryParts.iterator();
//		while(it.hasNext())
//		{
//			Match match = it.next();
//			MatchType type = match.getType();
//			switch (type){
//				case MEASURE : measures.add(match);
//				break;
//				case ATTRIBUTE : dimensions.add(match);
//				break;
//				default: break;
//		}
//		}*/
//		
//		String [] fields = {"session", "userId", "question", "suggestions", "chosenM", "chosenD", "chosenF", "analysis"};
//		
//		values += measures.toString()+"','"+dimensions.toString()+"','"+filters.toString()+"','"+analyseId+"'";
//		
//		String insertObs = insert("BIG_1_LOGS", fields, values);
//		ds.getJdbcTemplate().getJdbcOperations().execute(insertObs); 
//	}
	
	
	public static String insert(String table, String[] fields, String values) {
		StringBuilder buffer = new StringBuilder("INSERT INTO ");
		buffer.append("\"").append(table).append("\"");
		return insert(buffer, fields, values);
	}

	public static String insert(String[] table, String[] fields, String values) {
		StringBuilder buffer = new StringBuilder("INSERT INTO ");
		appendTableName(buffer, table);
		return insert(buffer, fields, values);
	}

	private static  String insert(StringBuilder buffer, String[] fields, String values) {
		if (fields != null && fields.length > 0) {
			buffer.append(" ( ");
			appendFields(buffer, fields);
			buffer.append(" ) ");
		}
		buffer.append(" VALUES ( ").append(values).append(")");
		return buffer.toString();
	}
	
	protected static void appendTableName(StringBuilder buffer, String[] table) {
		buffer.append("\"");
		boolean first = true;
		for (int i = 0; i < table.length; i++) {
			String tn = table[i];
			if (tn != null) {
				tn = tn.trim();
				if (first) {
					first = false;
				} else {
					buffer.append("\".\"");
				}
				buffer.append(tn);
			}
		}
		buffer.append("\"");
	}
	
	private static void appendFields(StringBuilder buffer, String[] fields) {
		for (int i = 0; i < fields.length; i++) {
			if (i > 0) {
				buffer.append(", ");
			}
			buffer.append("\"").append(fields[i]).append("\"");
		}
	}
	
	public Context emptyContext (Context context)
	{
		ArrayList<Event> obs = context.getevents();
		obs.clear();
		context = new Context(obs);
		return context;
	}
	
	
		
		
	

}
