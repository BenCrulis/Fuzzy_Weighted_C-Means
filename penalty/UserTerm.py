"""
Implements a user oriented penalty term.

The user is considered as a numpy vector, each dimension being the weight of the user into a specific topic
The similarity with a user is basically a cosine measure between the vector representative of the user
and the vectors representative of the bundle.
The hypothesis is that a user and a query from the bundle can be represented seamlessly in the same space.
"""
from penalty.Penalty import Penalty
import numpy as np

class UserTerm(Penalty):

    def __init__(self, user, sim):
        """
        :param user: vector of topics weights that characterize the user
        :param sim: similarity function between a user and a query of a bundle (cos, 1 - jaccard)
        """
        Penalty.__init__(self)
        self.user = user
        self.sim = sim

    def evaluate(self, bundle):
        """
        Similarity between bundle and a user as the sum of similarities between queries in the bundle and the user
        :param bundle: a set of query vectors
        :return: a real value representing the similarity of the user with the bundle
        """
        size = np.shape(bundle)[0]
        sum = 0
        for i in range(size):
            sum += self.sim(bundle[i], self.user)
        return sum

    def evaluateSingle(self, query):
        """
        Similarity between a single query and a user vector
        :param query: query vector
        :return: a real value representing the similarity of the user with a single query
        """
        return self.sim(query, self.user)