package dataStructures;

public class HierarchicalLevel {
	
	private String universe;
	private String navPath;
	private String object;
	
	public HierarchicalLevel()
	{
		universe="";
		navPath="";
		object="";
	}
	
	public HierarchicalLevel(String u, String np, String o)
	{
		this.universe = u;
		this.navPath = np;
		this.object = o;
	}

	public String getUniverse() {
		return universe;
	}

	public void setUniverse(String universe) {
		this.universe = universe;
	}

	public String getNavPath() {
		return navPath;
	}

	public void setNavPath(String navPath) {
		this.navPath = navPath;
	}

	public String getObject() {
		return object;
	}

	public void setObject(String object) {
		this.object = object;
	}
	
	

}
