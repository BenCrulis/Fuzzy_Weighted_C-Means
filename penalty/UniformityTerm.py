"""
Implements a cluster center oriented penalty term.

The similarity with a user is basically a cosine (or any other) measure between the vector representative of the cluster
and the vectors representative of the bundle.
Hypothesis: a cluster center and a query from the bundle can be represented seamlessly in the same space.
"""
from penalty.Penalty import Penalty
import numpy as np

class UniformityTerm(Penalty):

    def __init__(self, sim):
        """
        :param center: vector of topics weights that characterize a cluster center
        :param sim: similarity function between a user and a query of a bundle (cos, 1 - jaccard)
        """
        Penalty.__init__(self)
        self.sim = sim

    def evaluate(self, center, bundle):
        """
        Similarity between bundle and a user as the sum of similarities
        between queries in the bundle and the cluster center
        :param center: cluster center for which computing uniformity
        :param bundle: a set of query vectors
        :return: a real value representing the similarity of the cluster center with the bundle
        """
        size = np.shape(bundle)[0]
        sum = 0
        for i in range(size):
            sum += self.sim(bundle[i], self.center)
        return sum

    def evaluateSingle(self, query):
        """
        Similarity between a query and a cluster center
        :param query: query vector
        :return: a real value representing the similarity of the cluster center with a single query
        """
        return self.sim(query, self.center)