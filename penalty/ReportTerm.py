from penalty.Penalty import Penalty
from utility.comparison import cos
import numpy as np

class ReportTerm(Penalty):

    def __init__(self, report, sim):
        """
        :param report: set of vectors of topics weights that characterize the queries in the report
        :param sim: similarity function between a user and a query of a bundle (cos, 1 - jaccard)
        """
        Penalty.__init__(self)
        self.report = report
        self.sim = sim

    def evaluate(self, bundle):
        """
        Similarity between bundle and a user as the sum of similarities between queries in the bundle and the queries
        from the bundle
        :param bundle: a set of query vectors
        :return: a real value representing the similarity of the report with the bundle
        """
        bsize = np.shape(bundle)[0]         # bundle size
        rsize = np.shape(self.report)[0]    # report size
        sum = 0
        for i in range(bsize):
            for j in range(rsize):
                sum += self.sim(bundle[i], self.report[j])
        return sum / (bsize * rsize)

    def evaluateSingle(self, query):
        """
        Similarity between a query and a report
        :param query: query vector
        :return: a real value representing the similarity of the user with the bundle
        """
        return self.sim(query, self.report)