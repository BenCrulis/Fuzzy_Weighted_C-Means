"""
Class FCM for Fuzzy C-Means clustering algorithm
Versions
0.1:Nicolas Labroche:03/23/2018:first complete version
"""

from algorithms.KM import KM
import numpy as np


class FCM(KM):

    def __init__(self, c, m = 2, epsilon = 10e-5, max_nb_iter = 100):
        """
        Default constructor
        :param c: number of clusters
        :param epsilon: convergence threshold
        :param max_nb_iter: maximal number of iterations
        """
        KM.__init__(self, c, epsilon, max_nb_iter)
        self.m = m
        # all other parameters are inherited from K-Means
    # end __init__

    def randomInitCenters(self):
        index = np.random.randint(0,self.n, self.k)
        self.centers = self.data[index]

    def randomInitMembership(self):

        self.membership = np.random.random((self.k,self.n))
        sum = np.sum(self.membership,axis=0)

        # alternate programming
        for i in range(self.n):
            if sum[i] != 0:
                self.membership[:,i] = (1.0 / sum[i]) * self.membership[:,i]
    # end randomInitMembership

    def innerTerm(self, i, j, distances) -> float:
        """
        Compute the 'raw' membership between object i and cluster j
        Hypothesis: distance matrix is up to date
        :param i: index of object
        :param j: index of cluster
        :param distances: distances as a k*n matrix
        :return: 'raw' membership (before handling problems like membership is infinite)
        """
        p = 1 / (self.m - 1)

        mysum = 0 #sum is a builtin can't call variable sum renamed to mysum
        for k in range(self.k):
            t = distances[j][i] / distances[k][i]
            t = pow(t,p)
            mysum += t

        #mysum = sum(map(lambda x: pow(distances[j][i] / distances[x][i], p), range(self.k) ))

        if mysum == 0:
            return float('inf')
        else:
            return 1.0 / mysum


    def updateMembership(self):
        """
        This function updates clusters membership based on current centers coordinates
        The objective function value is updated as the sum of squared Euclidian
        distance assignment for each point weighted by the membership of each point to each cluster
        :return: None
        """
        self.J = 0

        # update distance matrix between points and clusters
        distances = [[self.distance(self.data[i], self.centers[j]) for i in range(self.n)] for j in range(self.k)]

        # init membership matrix
        self.membership = np.zeros((self.k, self.n))

        for i in range(self.n):
            warning = -1
            sumOverK = 0
            for j in range(self.k):
                self.membership[j][i] = self.innerTerm(i,j,distances)
                if self.membership[j][i] == float('inf'):
                    warning = j
                    break

            # if warning == -1:           # if no infinite membership
            #     sum = np.sum(self.membership[:,i], axis=0)              # TODO: to be checked!
            #     if abs(sum - 1) > 10e-6:
            #         print("Normalisation requise")
            #     # normalisation round 2
            #     for j in range(self.k):
            #         self.membership[j][i] = self.membership[j][i] / sum
            # else:
            if warning >= 0:
                self.membership[:, i] = np.zeros(self.k)
                self.membership[warning, i] = 1

        # computing the J value based on objective function Equation
        self.J = 0
        for j in range(self.k):
            self.J += np.dot(np.power(self.membership[j],self.m), distances[j])
    # end updateMembership()

    def updateCenters(self):
        """
        This function updates clusters' centers based on matrices products provided by numpy
        :return: None
        """
        # self.centers = np.zeros((self.k, self.natt))
        # the next instruction can be optimized by storing a transverse matrix for membership
        self.centers = np.matmul(np.power(self.membership,self.m),self.data)
        # number of elements in each cluster, ie. lines of membership matrix (axis=1)
        weights = np.sum(np.power(self.membership,self.m),axis=1)

        # alternate programming
        for j in range(self.k):
            if weights[j] != 0:
                self.centers[j] = (1 / weights[j]) * self.centers[j]

    def fit(self, dataset):
        """
        Build the k clusters following traditional fc-means approach.
        :param dataset: a nxm list of lists or ndarray
        :return: updates the fuzzy membership matrix that indicates for each point
                    to which extent it belongs to the clusters
        """

        self.data = np.asarray(dataset)
        self.n = self.data.shape[0]
        self.natt = self.data.shape[1]

        # Initialize random membership to clusters
        self.randomInitMembership()

        # Compute initial centers
        self.updateCenters()

        # self.randomInitCenters()                  # causes some problems when computing innerTerm because of div by 0

        nbiter = 0
        previousJ = float('inf')
        self.updateMembership()  # update membership and J value based on initial centers

        while nbiter <= self.max_iter and abs(previousJ - self.J) > self.epsilon:
            nbiter += 1                     # one more iteration
            self.updateCenters()            # update centers coordinates
            previousJ = self.J              # storing previous J value
            self.updateMembership()         # update membership and J value

    # end fit()

    def getJ(self):
        """
        This function returns the value of the objective function on this run
        :return: J value
        """
        return self.J

    def getClusterCenters(self):
        """
        This function returns the coordinates of the cluster centers as a (k clusters x m features) numpy ndarray
        :return:
        """
        return self.centers

    def getClusters(self):
        """
        Returns a 1D numpy array that indicates for each point the index of its cluster between 0 and k-1 included
        :return: 1D numpy array
        """
        res = np.zeros(self.n)
        for i in range(self.n):
            res[i] = np.argmax(self.membership[:,i])
        return res