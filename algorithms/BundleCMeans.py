"""
Class BundleCMeans that implements Cosine-based Fuzzy C-Means clustering algorithm
with bundle construction
The overall structure of the algorithm is the same as presented in
Alsayasneh et al. TKDE 2017. Personalized and Diverse Task Composition in Crowd Sourcing.

Versions
0.1:Nicolas Labroche:03/23/2018:
"""
from algorithms.FCM import FCM
import numpy as np
from penalty.DiversityTerm import DiversityTerm
from penalty.UserTerm import UserTerm
from penalty.UniformityTerm import UniformityTerm
from utility.comparison import cos
from sklearn.metrics.pairwise import cosine_similarity

class BundleCmeans(FCM):

    def __init__(self, c, user, budget, sim=cos, eta = 10, m=0.8, epsilon=10e-5, max_nb_iter=100):
        """
        Default constructor
        :param c: number of clusters
        :param user: vector representative of the user
        :param budget: budget of each bundle
        :param sim: similarity measure used for "distance" computation
        :param eta: number of cycles to adjust the weights of the optimization function
        :param m: fuzzy exponent 'm'
        :param epsilon: convergence threshold
        :param max_nb_iter: maximal number of iterations
        """
        FCM.__init__(self, c, m, epsilon, max_nb_iter)
        self.budget = budget  # budget = #w in the TKDE 2017 paper
        # (number of tasks that a worker would usually perform)

        self.user = user        # reference to the user profile in the topic space
        self.eta = eta          # number of cycles to learn the weights of the optimization function
        self.sim = sim          # similarity measure used in all "distance" computations

        # weights of each term of the optimization function - to be set by the fit() function

        self.alpha = 0
        self.beta = 0
        self.gamma = 0
        self.delta = 0

        # setting penalty terms
        self.diversity = DiversityTerm(cos)
        self.personalization = UserTerm(user, cos)
        self.uniformity = UniformityTerm(cos)

        self.bundles = None     # bundles are represented as List of List in Python for convenience
                                # (allow for a simple filtering of the dataset)
    # end __init__

    def randomInitMembership(self):

        tmp = np.random.random((self.n,self.k))
        sum = np.sum(tmp,axis=1)
        self.membership = np.divide(np.transpose(tmp), sum)

    def randomInitCenters(self):
        index = np.random.randint(0,self.n, self.k)
        self.centers = self.data[index]

    def innerTerm(self, i, j, distances):
        """
        Compute the 'raw' membership between object i and cluster j
        Hypothesis: distance matrix is up to date
        :param i: index of object
        :param j: index of cluster
        :param distances: distances as a k*n matrix
        :return: 'raw' membership (before handling problems like membership is infinite)
        """
        p = 1 / (self.m - 1)
        sum = 0
        for k in range(self.k):
            t = distances[j][i] / distances[k][i]
            t = pow(t,p)
            sum += t
        if sum == 0:
            return float('inf')
        else:
            return 1 / sum



    def updateMembership(self):
        """
        This function updates clusters membership based on current centers coordinates
        The objective function value is updated for each point as the sum of similarities
        weighted by the membership of each point to each cluster
        :return: None
        """
        #self.J = 0

        # update distance matrix between points and clusters
        # distances = [[self.distance(self.data[i], self.centers[j]) for i in range(self.n)] for j in range(self.k)]

        # init membership matrix
        self.membership = np.zeros((self.k, self.n))

        for i in range(self.n):
            warning = -1
            sumOverK = 0
            for j in range(self.k):
                self.membership[j][i] = self.innerTerm(i,j,self.distances)
                if self.membership[j][i] == float('inf'):
                    warning = j
                    break

            if warning >= 0:
                self.membership[:, i] = np.zeros(self.k)
                self.membership[warning, i] = 1
    # end updateMembership()

    def updateObjective(self,alphap,betap,deltap,gammap):
        """
        This function updates the value of the objective function based on the self.memberships and self.bundles
        :return: a real value for the objective function
        """

        # computing first term
        self.J = 0
        for j in range(self.k):
            self.J += np.dot(np.power(self.membership[j], self.m), self.distances[j])

        # normalization of first term
        self.J = (alphap / self.n) * self.J

        # evaluation penalty term for each cluster
        score = 0
        if self.bundles is not None:
            for j in range(self.k):
                bundle_j = self.data[self.bundles[j]]
                score = betap * self.personalization.evaluate(bundle_j)
                score += (deltap / self.budget - 1) * self.diversity.evaluate(bundle_j)
                score += gammap * self.personalization.evaluate(bundle_j)

            score = (1/(self.k * self.budget)) * score          # normalization

        self.J += score


    def updateBundles(self,alphap,betap,deltap,gammap):
        """
        Function that builds one candidate bundle for each cluster with a greedy approach.
        :return: sets or updates the bundle matrix
        """

        self.bundles = []  # list of list of index. Each internal list of index is a bundle
        for j in range(self.k):
            self.bundles.append([])
            for item in range(self.budget):
                # each iteration adds a new item to bundle for cluster j
                best = -1
                index = -1

                for i in range(self.n):
                    if i not in self.bundles[j]:
                        score_i = betap * self.distances[j][i] + \
                                  gammap * self.personalization.evaluateSingle(self.data[i])
                        if len(self.bundles[j]) > 1:
                            # Sw[j] is a Python list containing the index of points that belongs to the jth bundle
                            # self.data[Sw[j]] should allow to retrieve only the queries that are related to the bundle 'j'
                            tmp = list(self.bundles[j])                     # possible to move this instruction to optimize things?
                            tmp.append(i)                                   # adding the new potential point to bundle
                            score_i += (1 / len(tmp)) * deltap * \
                                       self.diversity.evaluate(self.data[tmp])

                        if score_i > best:
                            best = score_i
                            index = i
                # No element to add to the bundle
                if best == -1:
                    raise LookupError("Impossible to find an element for the bundle")
                else:
                    self.bundles[j].append(index)
                # end loop dataset
            # end loop budget
        # end loop for each cluster


    def bundleListToMatrix(self):
        """
        This function returns a crisp matrix that represents bundle membership
        :return: a binary matrix for crisp membership
        """
        bund = np.zeros((self.k, self.n))
        if self.bundles != None:
            for j in range(self.k):
                for i in self.bundles[j]:
                    bund[j][i] = 1
        return bund


    def updateCentersEuclidian(self):
        """
        This function updates clusters' centers following the penalty term and the use of Euclidian distance measure
        :return: None
        """
        # the next instruction can be optimized by storing a transverse matrix for membership
        self.centers = np.matmul(self.membership, self.data)
        # weights this temporary matrix
        self.centers = (self.alpha / self.X) * self.centers
        # computing the penalty term

        bund = self.bundleListToMatrix()

        penalty = np.matmul(bund, self.data)
        # weighting the penalty term
        penalty = (self.beta / self.budget) * penalty

        self.centers = np.add(self.centers, penalty)

        # number of elements in each cluster, ie. lines of membership matrix (axis=1)
        weights = (self.alpha / self.X) * np.sum(self.membership, axis=1)
        # TODO: check if this is correct, not sure about the dimensions of matrices in what follows
        weights = weights + self.beta * np.ones((1, self.membership.shape[1]))

        # normalizing centers: computing average
        tmp = np.transpose(self.centers)
        tmp = np.divide(tmp, weights)
        self.centers = np.transpose(tmp)

    def updateCenters(self, alphap, betap, deltap, gammap):
        """
        This function updates clusters' centers following the penalty term and the use of cosine similarity measure
        :return: None
        """

        ## STEP 1: compute Ajr for all clusters 'j' and feature dimension 'r'

        # self.centers = np.zeros((self.k, self.natt))
        # the next instruction can be optimized by storing a transverse matrix for membership
        self.centers = np.matmul(np.power(self.membership, self.m), self.data)
        # weights this temporary
        self.centers = (alphap / self.n) * self.centers
        # computing the penalty term

        bund = self.bundleListToMatrix()

        penalty = np.matmul(bund, self.data)
        # weighting the penalty term
        penalty = (betap / self.budget) * penalty

        self.centers = np.add(self.centers, penalty)

        ## STEP 2: normalization
        A2jr = np.power(self.centers, 2)

        # sum over the features dimension 'r'
        A2jr = np.sum(A2jr, axis=1)
        A2jr = np.power(A2jr, 0.5)

        # normalizing centers: computing average
        for j in range(self.k):
            self.centers[j] = self.centers[j] * (1 / A2jr[j])


        # tmp = np.transpose(self.centers)
        # tmp = np.divide(tmp, A2jr)
        # self.centers = np.transpose(tmp)

        ## done

    def fit_iter(self, dataset, alphap, betap, deltap, gammap):
        """
        Build k bundles following the approach proposed in TKDE 2017: only for specified
        value of alpha, beta, delta and gamma. Corresponds to the inner loop of the
        algorithm in TKDE 2017.
        When entering this function initial centers are read into self.centers
        :param dataset: a nxm list of lists or ndarray
        :param alphap: weight of traditional FCM term in the objective function
        :param betap: uniformity weight
        :param deltap: diversity weight
        :param gammap: personalisation weight
        :return: updates the membership matrix that indicates for each point the cluster it belongs to
        and the binary bundle membership matrix as well as cluster centers coordinates
        """

        # self.updateDistanceMatrix()
        self.updateObjective(alphap, betap, deltap, gammap)
        nbiter = 0
        previousJ = float('inf')

        while nbiter <= self.max_iter and abs(previousJ - self.J) > self.epsilon:
            nbiter += 1                                                     # one more iteration
            previousJ = self.J                                              # storing previous J value
            self.updateMembership()                                         # update membership and J value
                                                                            # based on initial centers
            self.updateBundles(alphap, betap, deltap, gammap)               # update bundles based on each cluster
            self.updateCenters(alphap, betap, deltap, gammap)               # update centers coordinates
            self.updateDistanceMatrix()

            # update objective function value
            previousJ = self.J
            self.updateObjective(alphap, betap, deltap, gammap)

    # end fit()

    def fit(self, dataset, alpha, beta, delta, gamma):
        """
        General algorithm that scans the possible alpha, beta, gamma, delta to find the best bundles
        starting from the provided values
        :param dataset: dataset from which scanning for bundles
        :return: a set of bundles
        """
        self.alpha = alpha
        self.beta = beta
        self.delta = delta
        self.gamma = gamma

        alphap = self.alpha + self.beta + self.gamma + self.delta
        betap = 0
        deltap = 0
        gammap = 0

        J = []              # eta (nb. of cycles) values of objective function: warning: each value cannot be compared to the other one
                            # as the optimization function changes
        B = []              # list of discovered bundle at each cycle
        P = []              # List of parameters alpha, beta, delta, gamma for each cycle

        self.data = np.asarray(dataset)
        self.n = self.data.shape[0]
        self.natt = self.data.shape[1]

        # Initialize random membership to clusters and compute initial centers
        #self.randomInitMembership()
        #self.updateCenters(alphap, betap, deltap, gammap)
        self.randomInitCenters()
        self.updateDistanceMatrix()
        self.updateMembership()

        for cycle in range(self.eta):
            self.fit_iter(dataset,alphap, betap,deltap,gammap)
            P.append([alphap, betap,deltap,gammap])
            B.append(self.bundles)                  # TODO: warning here: copy bundles or only reference?
            J.append(self.J)
            # update parameters
            alphap = alphap - ((self.beta + self.delta + self.gamma)/self.eta)
            betap = betap + (self.beta/self.eta)
            deltap = deltap + (self.delta / self.eta)
            gammap = gammap + (self.gamma / self.eta)
        print("Finished")

    def updateDistanceMatrix(self):
        # distances = np.zeros((self.k, self.n))
        # self.distances = [[self.sim(self.data[i], self.centers[j]) for i in range(self.n)] for j in range(self.k)]
        self.distances = cosine_similarity(self.centers, self.data)
