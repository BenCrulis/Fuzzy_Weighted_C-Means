package dataStructures;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class Event {
	

	private String timestamp;
	private String sessionID;
	private String userID;
	private String eventID;
	private String eventType;
	private String docID;
	private ArrayList<String> docMeasures;
	private ArrayList<String> docDimensions;
	private HashMap<String, ArrayList<String>> filters;
	private ArrayList<String> universID;
	private String folderID;
	private HashMap<String, ArrayList<String>> eventDetails;
	
String [] fields = {"timestamp", "sessionId", "userId", "eventId", "eventType", "docId", "folderId", "eventDetailType", "eventDetailValue"};
	
	
	
	public Event()
	{
		this.timestamp = "";
		this.sessionID = "";
		this.universID = new ArrayList<String>();
		this.userID = "";
		this.eventID = "";
		this.eventType = "";
		this.docID = "";
		this.docMeasures = new ArrayList<String>();
		this.docDimensions = new ArrayList<String>();
		this.filters = new HashMap<String, ArrayList<String>>();
		this.folderID = "";
		this.eventDetails = new HashMap<String, ArrayList<String>>();
		
	}
	
	
	//README: use this constructor to take the values from the databases and create an Event 
	public Event (String time, String session, String user, String eventId, String eventType, String doc, String folder,HashMap<String, ArrayList<String>> eventDetails, Connection con) throws SQLException
	{
		
		this.timestamp = time;
		this.sessionID = session;
		this.folderID = folder;
		this.userID = user;
		this.eventID = eventId;
		this.eventType = eventType;
		this.docID = doc;
		
//		Connection con = null;
		ResultSet rs = null ;

	//	SQLServerDataSource ds = new SQLServerDataSource();
//		String connectionUrl = "jdbc:postgresql://[::1]:5432/audit";
//		con = DriverManager.getConnection(connectionUrl);

		Statement stmt = con.createStatement();
		// MEASURES extacted from doc
		String query = "SELECT detailname FROM docdetails WHERE detailtype=3 AND objectid='"+doc+"'";
		rs = stmt.executeQuery(query);
		this.docMeasures = new ArrayList<String>();
		while(rs.next())
		{
			this.docMeasures.add(rs.getString(1));
		}
		
		// DIMENSIONS extacted from doc
		String query1 = "SELECT DISTINCT detailname FROM docdetails WHERE detailtype=2 AND objectid='"+doc+"'";
		rs = stmt.executeQuery(query1);
		this.docDimensions = new ArrayList<String>();
		while(rs.next())
			this.docDimensions.add(rs.getString(1));
		
		// FILTERS extracted from doc  
		String query2 = "SELECT DISTINCT detailname, detailvalue FROM docdetails WHERE detailtype=1 AND objectid='"+doc+"'";
		rs = stmt.executeQuery(query2);
		this.filters = new HashMap<String, ArrayList<String>>();
		while(rs.next())
		{
			if(this.filters.containsKey(rs.getString(1)))
			{
				ArrayList<String> objects = this.filters.get(rs.getString(1));
				objects.add(rs.getString(2));
				this.filters.replace(rs.getString(1), objects);
			}
			else
			{
				ArrayList<String> object = new ArrayList<String>();
				object.add(rs.getString(2));
				this.filters.put(rs.getString(1),object );
			}
		}
		
		//UNIVERSES included in a doc
		String query3 = "SELECT DISTINCT parentid, universecuid from auditdb WHERE EVENTDETAILTYPE = 'Universe ID' AND EVENTDETAILVALUE<>' ' AND parentid='"+doc+"' ORDER BY parentid	";
		rs = stmt.executeQuery(query3);
		this.universID = new ArrayList<String>();
		while(rs.next())
			this.universID.add(rs.getString(2));
		
		
		//PROMTS from the event as they are not stored in the document and it depends from the reports opended during an event

		this.eventDetails = eventDetails;
		
	}



	
	
	public String getTimestamp() {
		return timestamp;
	}


	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}


	public String getSessionID() {
		return sessionID;
	}


	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}


	public String getUserID() {
		return userID;
	}


	public void setUserID(String userID) {
		this.userID = userID;
	}


	public String getEventID() {
		return eventID;
	}


	public void setEventID(String eventID) {
		this.eventID = eventID;
	}


	public String getEventType() {
		return eventType;
	}


	public void setEventType(String eventType) {
		this.eventType = eventType;
	}


	public String getDocID() {
		return docID;
	}


	public void setDocID(String docID) {
		this.docID = docID;
	}


	public ArrayList<String> getDocMeasures() {
		return docMeasures;
	}


	public void setDocMeasures(ArrayList<String> docMeasures) {
		this.docMeasures = docMeasures;
	}


	public ArrayList<String> getDocDimensions() {
		return docDimensions;
	}


	public void setDocDimensions(ArrayList<String> docDimensions) {
		this.docDimensions = docDimensions;
	}


	public HashMap<String, ArrayList<String>> getFilters() {
		return filters;
	}


	public void setFilters(HashMap<String, ArrayList<String>> filters) {
		this.filters = filters;
	}


	public ArrayList<String> getUniversID() {
		return universID;
	}


	public void setUniversID(ArrayList<String> universID) {
		this.universID = universID;
	}


	public String getFolderID() {
		return folderID;
	}


	public void setFolderID(String folderID) {
		this.folderID = folderID;
	}


	public HashMap<String, ArrayList<String>> getEventDetails() {
		return eventDetails;
	}


	public void setEventDetails(HashMap<String, ArrayList<String>> eventDetails) {
		this.eventDetails = eventDetails;
	}


	@Override
	public String toString() {
		return "Event [timestamp=" + this.getTimestamp() + ", session="
				+ this.getSessionID() + ", user=" + this.getUserID() + ", eventType=" + this.eventType + ", folder=" + this.folderID+  ", documentID=" + this.getDocID() + ", universes="
				+ this.getUniversID().toString() + ", measures=" + this.getDocMeasures() + ", dimensions=" + this.getDocDimensions() + ", filters=" + this.getFilters() + ", prompts=" + this.getEventDetails() + "]";
	}
}
