package calculations;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Stream;

import javax.naming.NamingException;
import javax.sql.DataSource;

import context.DocUsage;
import dataStructures.Context;
import dataStructures.DateTime;
import dataStructures.Document;
import dataStructures.Event;
import dataStructures.Filter;
import dataStructures.HierarchicalLevel;
import dataStructures.Report;
import dataStructures.WebIBlock;

public class FeatureExtractionDocument {

	ArrayList<Document> contexts = new ArrayList<Document>();
	

	private static String[] featureDimensions = {"group",/* "user", "docobject",*/ "measure",/* "dimension", "filter", "event",/* "eventtype", "folder" ,*/ "universe","session", /*"viz", "titlekeywords", "positionSession",*/ "filtervalues" /* ,"period",  "popularity"*/};
	 
	// private static String[] featureDimensions = {"universe", "measure", "dimension", "filter", "user", "viz", "folder"};
	 private static String [] combContext;
	 private static Double[] weights = {0.75187782 , 0.26421008 , 0.71039214, -0.34686768, -0.39030578
};
	 static HashMap<String, String> lastUpdate ;


	//TODO: Make it worth
	 public FeatureExtractionDocument ()
	{
	}
	
	
	
	/** Calculation of featureDimensions matrice 
	 * Return a matrix where lines represent each couple of documents that we combine
	 * and each column, except the last one, correspond to dissimilarity between two
	 * documents, compared for each feature. Last column is a binary value that shows 
	 * if the two documents belong to the same class.
	 **/
	public float [][] calculMatrice(Document[] contexts, HashMap<String, Integer> maxRank, HashMap<HierarchicalLevel, Integer> npath, ArrayList<String[]> position, HashMap<String, Float> popularity, ArrayList<DocUsage> docUsage, Connection con, String usergroups, HashMap<String, ArrayList<String>> titles) throws FileNotFoundException, IOException, SQLException{
		
        int length = contexts.length;

	//	lastUpdate= updateLastVisited(contexts);
		
        int pos = 0; 
		int nbCombin = factorial(length);
		float [][]result = new float[nbCombin][featureDimensions.length+1];
		combContext = new String[nbCombin];
 		for (int i = 0; i < length - 1; i++) {
			for (int j = i + 1; j < length; j++) {
				combContext[pos] = "C"+(i+1)+"C"+(j+1);
				//combContext[pos] = "<"+contexts[i].getDocID()+"> - <"+contexts[j].getDocID()+">";
				for (int f = 0; f < featureDimensions.length; f++) {
					switch (featureDimensions[f]) {
					case "group":
						result[pos][f] = sameGroup(contexts[i],contexts[j], usergroups);
					break;
					case "user":
						result[pos][f] = sameUsers(contexts[i],contexts[j]);
					break;
			//		case "docobject" :
			//			result[pos][f] = (float) sameObjects(contexts[i],contexts[j] );
			//		break;
					case "eventtype":
						result[pos][f] = sameEventType(contexts[i],contexts[j]);
					break;
					case "folder":
						result[pos][f] = sameTopFolders(contexts[i], contexts[j]);
						break;
					case "universe":
						result[pos][f] = sameUniverses(contexts[j], contexts[i]);
						break;
					case "event":
						result[pos][f] = sameEvent(contexts[j], contexts[i]);
					break;
					case "session":
						result[pos][f] = sameSession(contexts[j], contexts[i]);
					break;
			//		case "period":
			//			result[pos][f] = samePeriod(contexts[j], contexts[i]);
			//		break;
			//		case "positionSession":
			//			result[pos][f] = samePositionInSession(contexts[j], contexts[i], position);
			//		break;
			//		case "popularity":
			//			result[pos][f] = samePopularity2(contexts[j], contexts[i], popularity);
			//		break;
			//		case "dimensions":
			//			result[pos][f] = (float) sameObjectsWithoutMeasures(contexts[j], contexts[i]);
			//		break;
					case "measure" :
						result[pos][f] = (float) sameMeasOrDim(contexts[i],contexts[j], "Measure");
					break;
					case "dimension" :
						result[pos][f] = (float) sameMeasOrDim(contexts[i],contexts[j], "Dimension");
					break;
					case "filter" :
						result[pos][f] = (float) sameFilter(contexts[i],contexts[j]);
					break;
					case "viz" :
						result[pos][f] = (float) sameViz(contexts[i],contexts[j]);
					break;
					case "titlekeywords" :
						result[pos][f] = (float) sameTitleKeywords(contexts[i],contexts[j], titles);
					break;
					default:
						System.out.println("No feature dimension !");
					}					
				}
	/*			if(contexts[i].getUniversID()!=null && contexts[j].getUniversID()!=null)
				{
			//	if(contexts[i].getUniversID().equals(contexts[j].getUniversID()))
			/************  1session -> 1 context *****************/
				/** Resent comparison with universes (sharing the half) **/
/*				ArrayList<String> copy2 = new ArrayList<String>();
				ArrayList<String> copy1 = new ArrayList<String>();
				for(String s : contexts[i].getUniversID() )
					copy1.add(s);
				for(String s2 : contexts[j].getUniversID() )
					copy2.add(s2);
				copy2.removeAll(copy1);
				if(copy2.isEmpty() || copy2.size()<(contexts[j].getUniversID().size()/2))
				      result[pos][featureDimensions.length] = (float) 1.0;
			    else result[pos][featureDimensions.length] = (float) -1.0;
			//	}
			//	 else result[pos][featureDimensions.length] = (float) -1.0;
				pos++;
*/				
			//	System.out.println();
 		
			/***** Two docs same interest if same User
			 **/
				HashMap<String, Integer> freqUser1 = null, freqUser2= null;
				for(DocUsage dc : docUsage)
				{
					if(freqUser1==null|| freqUser2==null)
					{
					if(dc.getIdDoc().equals(contexts[i].getDocID()))
						freqUser1=new HashMap<String, Integer>(dc.getUserFreq());
					if(dc.getIdDoc().equals(contexts[j].getDocID()))
						freqUser2 = new HashMap<String, Integer>(dc.getUserFreq());
					}
					else break;
					
				}
				String[][] userIDs = rankUsers(freqUser1, freqUser2);
				ArrayList<String> freq2 =  VectorToArray (userIDs[1]);
				double sim = 0.0;
				for(int val=0; val<userIDs[0].length; val++)
				{
					if(freq2.contains(userIDs[0][val]))
					{
						sim+= (double)(1- ( (double)(Math.abs(val - freq2.indexOf(userIDs[0][val])))/userIDs[0].length)) /(double) userIDs[0].length;
//						System.out.println(freq2.indexOf(userIDs[0][val]));
//						System.out.println(Math.abs(val - freq2.indexOf(userIDs[0][val])));
//						System.out.println((double)Math.abs(val - freq2.indexOf(userIDs[0][val]))/userIDs[0].length);
					}
				}
				if(sim>0.2)
					result[pos][featureDimensions.length] = (float) 1.0;
				else 
					result[pos][featureDimensions.length] = (float) -1.0;
				pos++;
						
			/*** Two docs -> same interest if same Usergroup & same Period  
			 **/
			/*	float usergr = sameGroup(contexts[i],contexts[j]);
				float period = samePeriod(contexts[j], contexts[i]);
				if(usergr>=0.5 && period >=0.5)
				      result[pos][featureDimensions.length] = (float) 1.0;
			    else result[pos][featureDimensions.length] = (float) -1.0;
				pos++;
			*/
				/*** Two docs -> same interest if same objetcts  
				 **/
			/*	float objects = (float) sameObjects(contexts[i],contexts[j],maxRank, npath );
				if(objects>=0.3)
				      result[pos][featureDimensions.length] = (float) 1.0;
			    else result[pos][featureDimensions.length] = (float) -1.0;
				pos++;*/
			}
		}
		return result;
	}
	



/**  To calculate the weights of our features, we take a file, containing some combinations of documents with the annotations of users (1/0), judging if the two documents correspond to the same user interest
 * 			1. To learn the weights we base only on the documents of the file
 * 			2. The ground truth recuperated from the file as well
 * 
 * Calculation of featureDimensions matrix 
	 * Return a matrix where lines represent each couple of documents that we combine
	 * and each column, except the last one, correspond to dissimilarity between two
	 * documents, compared for each feature. Last column is a binary value that shows 
	 * if the two documents belong to the same class.
	 * 
	 * @param contexts : couples of documents, we know apriori for each of them if they belong to the same class
	 * @param classif  : for each couple, the binary value 1/0 of classification
	 * @param usergroups : file that contains correspondences between user and usergroups
	 * @param docTit	:  file that have titles of document and its reports for all cuids
	 * 
	 * @return result - matrix of dissimilarity for each couple of documents and each feature.
	 * 
 * **/
public float [][] calculMatriceSelectedDocs(Document contexts[][], int classif[], String usergroups, HashMap<String, ArrayList<String>> titles, ArrayList<String[]> positions) throws FileNotFoundException, IOException, SQLException{
		
  
	

	float [][]result = new float[contexts.length][featureDimensions.length+1];
	
		combContext = new String[contexts.length];  // contains the documents combinations
		for(int i=0; i<contexts.length; i++)
		{
		
			if(contexts[i][0]!=null && contexts[i][1]!=null)
			{
	  			combContext[i] = contexts[i][0].getDocID()+" : "+contexts[i][1].getDocID();  // first column identifying the doc combination
				
				for (int f = 0; f < featureDimensions.length; f++) {
					switch (featureDimensions[f]) {
					case "group":
						result[i][f] = sameGroup(contexts[i][0],contexts[i][1], usergroups);
					break;
					case "user":
						result[i][f] = sameUsers(contexts[i][0],contexts[i][1]);
					break;
					case "event":
						result[i][f] = sameEvent(contexts[i][0],contexts[i][1]);
					break;
					case "eventtype":
						result[i][f] = sameEventType(contexts[i][0],contexts[i][1]);
					break;
					case "folder":
						result[i][f] = sameTopFolders(contexts[i][0],contexts[i][1]);
						break;
					case "universe":
						result[i][f] = sameUniverses(contexts[i][0],contexts[i][1]);
						break;
					case "session":
						result[i][f] = sameSession(contexts[i][0],contexts[i][1]);
					break;
					case "positionSession":
						result[i][f] = samePositionInSession(contexts[i][0],contexts[i][1], positions);
					break;
					case "filtervalues":
						result[i][f] = (float) sameFilterValues(contexts[i][0],contexts[i][1]);
					break;
					case "measure" :
						result[i][f] = (float) sameMeasOrDim(contexts[i][0],contexts[i][1], "Measure");
					break;
					case "dimension" :
						result[i][f] = (float) sameMeasOrDim(contexts[i][0],contexts[i][1], "Dimension");
					break;
					case "filter" :
						result[i][f] = (float) sameFilter(contexts[i][0],contexts[i][1]);
					break;
					case "viz" :
						result[i][f] = (float) sameViz(contexts[i][0],contexts[i][1] );
					break;
					case "titlekeywords" :
						result[i][f] = (float) sameTitleKeywords(contexts[i][0],contexts[i][1], titles );
					break;
						case "docobject" :
							result[i][f] = (float) sameObjects(contexts[i][0],contexts[i][1] );
						break;
//					case "period":
					//			result[i][f] = samePeriod(contexts[i][0],contexts[i][1]);
					//		break;
					default:
						System.out.println("No feature dimension !");
					}					
				}
	
					result[i][featureDimensions.length] = classif[i];
				
			}
		}
		return result;
	}
	
	
	


		private static float sameTitleKeywords(Document document, Document document2, HashMap<String, ArrayList<String>> titles) {
		
				ArrayList<String> t1 = new ArrayList<String>();
				ArrayList<String> t2 = new ArrayList<String>();
				float sim = 0;
				
				if(titles.containsKey(document.getDocID()) && titles.containsKey(document2.getDocID()))
				{
						for(String t : titles.get(document.getDocID()))
								t1.addAll(VectorToArray(t.split(" ")));
		
						for(String t : titles.get(document2.getDocID()))
								t2.addAll(VectorToArray(t.split(" ")));
		
				HashMap<String, Integer> title1 = new HashMap<String, Integer>();
				HashMap<String, Integer> title2 = new HashMap<String, Integer>();
		
		       if(!title1.isEmpty() && !title2.isEmpty())
		    	   return (float) 0.0;
		       
		       title1 = getFrequences(title1, t1);
		       title2 = getFrequences(title2, t2);
		
		HashSet<String> title = new HashSet<String>();
		
		title.addAll(title1.keySet());
		title.addAll(title2.keySet());		
		
		Integer[][] vectors = new Integer[2][title.size()];
		Arrays.fill(vectors[0], 0);
		Arrays.fill(vectors[1], 0);
		
		
		float similarity;
		int index = 0;
		for (String b : title){
			vectors[0][index] = title1.get(b) != null ? title1.get(b) : 0;
			vectors[1][index] = title2.get(b) != null ? title2.get(b) : 0;
			index++;
		}
		
		similarity = cosineQParts(vectors);
		
	   sim = (float) (similarity );
				}
		if(sim>0)
			return sim;
		return sim;
		
	}



	/** This feature calculates the similarity between two contexts(documents)
	 *  based on the different visualization used for their blocks.
	 *  
	 *  @param c1 first document
	 *  @param c2 second document
	 *  @return similarity score between the documents
	 *  	    based on viz used for their blocks.
	 */
	private static float sameViz(Document document1, Document document2) throws SQLException {
		
		float resultat=(float) 0.0;
		HashMap<String, Integer> viz1= new HashMap<String, Integer>(), viz2= new HashMap<String, Integer>();
		viz1 = document1.getViz();
		viz2 = document2.getViz();
		
		HashSet<String> viz = new HashSet<String>();
	
		viz.addAll(viz1.keySet());
		viz.addAll(viz2.keySet());
		
		Integer[][] v = new Integer[2][viz.size()];
		Arrays.fill(v[0], 0);
		Arrays.fill(v[1], 0);
		
		int index = 0;
		for (String type : viz){
			v[0][index] = viz1.get(type) != null ? viz1.get(type) : 0;
			v[1][index] = viz2.get(type) != null ? viz2.get(type) : 0;
			index++;
		}
	
		resultat = cosineQParts(v);
		
		return resultat;
	}
	

	/*** Returns the score of similarity between two documents
	 *   based on the groups of users that have used these documents.
	 *   
	 *   @param context :  first document
	 *   @param context2:  second document
	 *   
	 *   @return the score of dissimilarity between the groups of
	 *   users that have used these documents
	 * */
	private static float sameGroup(Document context, Document context2, String usergroups) throws FileNotFoundException, IOException {
		UserVectors uv = new UserVectors();
	
		HashMap<String, ArrayList<String>> userGroups = uv.userToGroups(new FileReader(usergroups));
		HashMap<String, Integer> user1 = context.getUserID();
		HashMap<String, Integer> user2 = context2.getUserID();
		ArrayList<String> group1 = new ArrayList<String>();
		ArrayList<String> group2 = new ArrayList<String>();
		
		Iterator it = user1.entrySet().iterator();
		while(it.hasNext())
		{
			Map.Entry pair = (Entry) it.next();
			if(userGroups.get((String)pair.getKey())!=null)
			{
				for(String g:userGroups.get((String)pair.getKey()))
					if(!group1.contains(g))
						group1.add(g);
			}
		}
		Iterator it2 = user2.entrySet().iterator();
		while(it2.hasNext())
		{
			Map.Entry pair = (Entry) it2.next();
			if(userGroups.get((String)pair.getKey())!=null)
			{
				for(String g:userGroups.get((String)pair.getKey()))
					if(!group2.contains(g))
						group2.add(g);
			}
		}
		
		
	//	return maxFractionArrays(group1, group2);
		return (float) maxFraction( group1, group2);
		
	}

	
	

	/** This feature calculates the similarity between two contexts(documents)
	 *  based on the sessions they are found together.
	 *  
	 *  @param c1 first document
	 *  @param c2 second document
	 *  @return similarity score between these documents
	 *  	    looking to the common sessions we found
	 *  		both of them.
	 */
	public static float sameSession (Document c1, Document c2)
	{
		ArrayList<String> sessions1 = c1.getSessionID();
		ArrayList<String> sessions2 = c2.getSessionID();
		
		return (float) maxFraction(sessions1, sessions2);
	}
	
	
	/*** Returns the score of similarity between two documents
	 *   based on the users that have used these documents.
	 *   
	 *   @param context :  first document
	 *   @param context2:  second document
	 *   
	 *   @return the score of dissimilarity between documents 
	 *   based on the users that have used them.
	 * */
	public static float sameUsers (Document c1, Document c2)
	{
		
		HashMap<String, Integer> users1 = c1.getUserID();
		HashMap<String, Integer> users2 = c2.getUserID();
		
		ArrayList<String> uniqueUsers = new ArrayList<String>();
		Iterator it = users1.entrySet().iterator();
		while(it.hasNext())
		{
			Map.Entry pair = (Entry) it.next();
			uniqueUsers.add((String) pair.getKey());
		}
		Iterator it2 = users2.entrySet().iterator();
		while(it2.hasNext())
		{
			Map.Entry pair = (Entry) it2.next();
			if(!uniqueUsers.contains((String)pair.getKey()))
			uniqueUsers.add((String) pair.getKey());
		}
	
			
			Integer[][] vectors = new Integer[2][uniqueUsers.size()];
			Arrays.fill(vectors[0], 0);
			Arrays.fill(vectors[1], 0);
			int index = 0;
			for (String measure : uniqueUsers){
				vectors[0][index] = users1.get(measure) != null ? users1.get(measure) : 0;
				vectors[1][index] = users2.get(measure) != null ? users2.get(measure) : 0;
				index++;
			}
		
		return (float) cosineQParts(vectors);
		
	}
	
	
	
	/**This feature calculates the similarity between two contexts(documents)
	 *  in terms of the objects used in documents. All the BOs are taken into 
	 *  account, but they have annotate with a different importance. Measures 
	 *  are the most important objects followed by dimensions and filters.
	 * 
	 * @param c1  first document
	 * @param c2  second document
	 * @return    the similarity between these documents based on the 
	 * 			  business objects they contain 
	 * @throws IOException
	 */
	public static float sameObjects(Document c1, Document c2  ) throws IOException {
		HashMap<String, Integer> measures1 = new HashMap<String, Integer>();
		HashMap<String, Integer> measures2 = new HashMap<String, Integer>();
		
		HashMap<String, Integer> dimensions1 =  new HashMap<String, Integer>();
		HashMap<String, Integer> dimensions2 = new HashMap<String, Integer>();
		
		HashMap<String, Integer> filters2 =  new HashMap<String, Integer>();
		HashMap<String, Integer> filters1 = new HashMap<String, Integer>();
		
		getObjects(c1, measures1, dimensions1, filters1);
		getObjects(c2, measures2, dimensions2, filters2);

        if(((!measures1.isEmpty() && !measures2.isEmpty()) || (!dimensions1.isEmpty() && !dimensions2.isEmpty()) || (!filters1.isEmpty() && !filters2.isEmpty()))==false)
            return (float) 0.0;

		
		HashSet<String> measures = new HashSet<String>();
		HashSet<String> dimensions = new HashSet<String>();
		HashSet<String> filters = new HashSet<String>();
		
		measures.addAll(measures1.keySet());
		measures.addAll(measures2.keySet());
		dimensions.addAll(dimensions1.keySet());
		dimensions.addAll(dimensions2.keySet());
		filters.addAll(filters1.keySet());
		filters.addAll(filters2.keySet());
		
		
		Integer[][] vectorsM = new Integer[2][measures.size()];
		Arrays.fill(vectorsM[0], 0);
		Arrays.fill(vectorsM[1], 0);
		Integer[][] vectorsD = new Integer[2][dimensions.size()];
		Arrays.fill(vectorsD[0], 0);
		Arrays.fill(vectorsD[1], 0);
		Integer[][] vectorsF = new Integer[2][filters.size()];
		Arrays.fill(vectorsF[0], 0);
		Arrays.fill(vectorsF[1], 0);
		
		float similarityM, similarityF, similarityD;
		int index = 0;
		for (String measure : measures){
			vectorsM[0][index] = measures1.get(measure) != null ? measures1.get(measure) : 0;
			vectorsM[1][index] = measures2.get(measure) != null ? measures2.get(measure) : 0;
			index++;
		}
		index = 0;
		for (String dimension : dimensions){
			vectorsD[0][index] = dimensions1.get(dimension) != null ? dimensions1.get(dimension) : 0;
			vectorsD[1][index] = dimensions2.get(dimension) != null ? dimensions2.get(dimension) : 0;
			index++;
		}
		index = 0;
		for (String att : filters){
			vectorsF[0][index] = filters1.get(att) != null ? filters1.get(att) : 0;
			vectorsF[1][index] = filters2.get(att) != null ? filters2.get(att) : 0;
			index++;
		}
		
		similarityM = cosineQParts(vectorsM);
		similarityD = cosineQParts(vectorsD);
		similarityF = cosineQParts(vectorsF);
		
		float similarity = (float) (similarityM * 0.5 + similarityD * 0.2 + similarityF * 0.3 );
		return similarity;
	}
	
	
	/**This feature calculates the similarity between two contexts(documents)
	 *  in terms of the measures/dimensions used in documents.
	 * 
	 * @param c1  first document
	 * @param c2  second document
	 * @param bo  String that identifies Measures or Dimensions
	 * @return    the similarity between these documents based on the 
	 * 			  measures/dimensions they contain 
	 
	 */
	public static float sameMeasOrDim(Document c1, Document c2, String bo  ) throws IOException {
		
		
		HashMap<String, Integer> bo1 = new HashMap<String, Integer>();
		HashMap<String, Integer> bo2 = new HashMap<String, Integer>();
		
		
		if(bo.equals("Measure"))
		{
		bo1 = getFrequences(bo1, c1.getDocMeasures());
		bo2 = getFrequences(bo2, c2.getDocMeasures());
		}
		else
		{
		bo1 = getFrequences(bo1, c1.getDocDimensions());
		bo2 = getFrequences(bo2, c2.getDocDimensions());
		}
		

        if(!bo1.isEmpty() && !bo2.isEmpty())
            return (float) 0.0;
		
		HashSet<String> bos = new HashSet<String>();
		
		bos.addAll(bo1.keySet());
		bos.addAll(bo2.keySet());		
		
		Integer[][] vectors = new Integer[2][bos.size()];
		Arrays.fill(vectors[0], 0);
		Arrays.fill(vectors[1], 0);
		
		
		float similarity;
		int index = 0;
		for (String b : bos){
			vectors[0][index] = bo1.get(b) != null ? bo1.get(b) : 0;
			vectors[1][index] = bo2.get(b) != null ? bo2.get(b) : 0;
			index++;
		}
		
		similarity = cosineQParts(vectors);
		
		float sim = (float) (similarity );
		return sim;
	}
	
	/**This feature calculates the similarity between two contexts(documents)
	 *  in terms of the filters used in documents.
	 * 
	 * @param c1  first document
	 * @param c2  second document
	 * @return    the similarity between these documents based on the 
	 * 			  filters they contain 
	 
	 */
	public static float sameFilter(Document c1, Document c2 ) throws IOException {
		
		
		HashMap<String, Integer> filter1 = new HashMap<String, Integer>();
		HashMap<String, Integer> filter2 = new HashMap<String, Integer>();
		
		filter1 = getFilterFreq(filter1, c1.getFilters());
		filter2 = getFilterFreq(filter2, c2.getFilters());
		
		

        if(!filter1.isEmpty() && !filter2.isEmpty())
            return (float) 0.0;
		
        HashSet<String> filters = new HashSet<String>();
        
		filters.addAll(filter1.keySet());
		filters.addAll(filter2.keySet());		
		
		Integer[][] vectors = new Integer[2][filters.size()];
		Arrays.fill(vectors[0], 0);
		Arrays.fill(vectors[1], 0);
		
		
		float similarity;
		int index = 0;
		for (String b : filters){
			vectors[0][index] = filter1.get(b) != null ? filter1.get(b) : 0;
			vectors[1][index] = filter2.get(b) != null ? filter2.get(b) : 0;
			index++;
		}
		
		similarity = cosineQParts(vectors);
		
		float sim = (float) (similarity );
		return sim;
	}
	
	
	/**This feature calculates the similarity between two contexts(documents)
	 *  looking to the types of the event they have been used to.
	 *
	 *	@param c1	first document	
	 *	@param c2	second document
	 * 	@return     the similarity score between documents, based on the
	 * 				type of the events they are related to.
	 */
	public static float sameEventType (Document c1, Document c2)
	{
	
		
		HashMap<String, Integer> u1 = c1.getEventType();
		HashMap<String, Integer> u2 = c2.getEventType();
		
		ArrayList<String> uniqueEvent = new ArrayList<String>();
		Iterator it = u1.entrySet().iterator();
		while(it.hasNext())
		{
			Map.Entry pair = (Entry) it.next();
			uniqueEvent.add((String) pair.getKey());
		}
		Iterator it2 = u2.entrySet().iterator();
		while(it2.hasNext())
		{
			Map.Entry pair = (Entry) it2.next();
			if(!uniqueEvent.contains(pair.getKey()))
			uniqueEvent.add((String) pair.getKey());
		}
			
			Integer[][] vectors = new Integer[2][uniqueEvent.size()];
			Arrays.fill(vectors[0], 0);
			Arrays.fill(vectors[1], 0);
			int index = 0;
			for (String measure : uniqueEvent){
				vectors[0][index] = u1.get(measure) != null ? u1.get(measure) : 0;
				vectors[1][index] = u2.get(measure) != null ? u2.get(measure) : 0;
				index++;
			}
		
		
		
		return cosineQParts(vectors);
	}
	
	
	/**This feature calculates the similarity between two contexts(documents)
	 *  looking if they are used in the same events.
	 *
	 *	@param c1	first document	
	 *	@param c2	second document
	 * 	@return     the similarity score between documents, based on the
	 * 				fraction of events they are used together.
	 */
	public static float sameEvent (Document c1, Document c2)
	{
		ArrayList<String> ev1 = c1.getEventID();
		ArrayList<String> ev2 = c2.getEventID();
		
		
		return (float) maxFraction(ev1, ev2);
	}
	
	
	/** This feature calculates the similarity between two contexts(documents)
	 *  in terms of the folders they are classified into.
	 *  
	 *  @param c1 first document
	 *  @param c2 second document
	 *  @return similarity score between the folders these documents
	 *  	    are classified into. In the case of only two documents,
	 *  		belonging each to one folder, it will be a boolean score.
	 */
	
	public static float sameTopFolders (Document c1, Document c2)
	{
		ArrayList<String> f1 = c1.getFolderID();
		ArrayList<String> f2 = c2.getFolderID();
		
		
		return (float) maxFraction(f1, f2);
	}
	
	
	
	/** This feature calculates the similarity between two contexts(documents)
	 *  looking the universes they use.
	 *  
	 *  @param c1 first document
	 *  @param c2 second document
	 *  @return similarity score between documents, calculated based on the 
	 *  		common fraction of universes they share.
	 */  
	public static float sameUniverses (Document c1, Document c2)
	{
		ArrayList<String> universes1 = c1.getUniversID();
		universes1.remove("<null>");
		ArrayList<String> universes2 = c2.getUniversID();
		universes2.remove("<null>");
		
		return (float) maxFraction(universes1, universes2);
	}
	

	



	/**
	 * Calculates the frequencies of business objects
	 * in a document or group of documents(context)
	 * @param occurences	HashMap where the result will be added
	 * @param entries		all the BO of a certain type extracted from the document(s)
	 * @return	the occurrences, structure in parameter with the frequencies of the BO
	 * 			in the document(s)
	 */
	public static HashMap<String, Integer> getFrequences(HashMap<String, Integer> occurrences, ArrayList<String> entries)
	{
		
		if(entries!=null)
		{
		for (String str : entries)
		{	
			if (occurrences.containsKey(str.toLowerCase()))
				occurrences.put(str.toLowerCase(), occurrences.get(str.toLowerCase()) + 1);
			else
				occurrences.put(str.toLowerCase(), 1);
		}
		}
		return occurrences;
	}
	
	
	
	/**
	 * The same as getFrequencies, but it compares the name and the values
	 * of the filters as well.
	 * @param occurences	HashMap passed in parameters to gather the frequencies
	 * 						of the BO
	 * @param entries		All BOs of a certain type retrieved from the document(s)
	 * @return				Document's objects frequencies
	 */
/*	public static HashMap<Filter, Integer> getFilterFreq(HashMap<Filter, Integer> occurences, HashMap<String, ArrayList<String>> entries)
	{
		if(entries!=null)
		{
		Iterator it = entries.entrySet().iterator();
		while(it.hasNext())
		{
			Map.Entry pair = (Map.Entry)it.next();
			Filter f = new Filter((String)pair.getKey(),(ArrayList<String>) pair.getValue());
			String str = (String)pair.getKey();
			if (occurences.containsKey(str))
				occurences.put(f, occurences.get(str) + 1);
			else
				occurences.put(f, 1);
		}
		}
		return occurences;
	}*/
	
	public static HashMap<String, Integer> getFilterFreq(HashMap<String, Integer> occurences, HashMap<String, ArrayList<String>> entries)
	{
		if(entries!=null)
		{
		Iterator it = entries.entrySet().iterator();
		while(it.hasNext())
		{
			Map.Entry pair = (Map.Entry)it.next();
			String str = (String)pair.getKey();
			if (occurences.containsKey(str))
				occurences.put(str, occurences.get(str) + 1);
			else
				occurences.put(str, 1);
		}
		}
		return occurences;
	}
	
	
	
	
	/**
	 * Fills the HashMaps passed in parameters with the 
	 * frequences of each business object of the document c
	 * @param c				the document
	 * @param measures		HashMap of measures - their frequences in doc c
	 * @param dimensions	HashMap of dimensions - their frequences in doc c
	 * @param filters		HashMap of filters - their frequences in doc c
	 */
	public static void getObjects(Document c, HashMap<String, Integer> measures, HashMap<String, Integer> dimensions, HashMap<String, Integer> filters)
	{
		
			measures = getFrequences(measures, c.getDocMeasures());
			dimensions = getFrequences(dimensions, c.getDocDimensions());
			filters = getFilterFreq(filters, c.getFilters());
		
	}
	
	
	/***	Generic function that find the maximum fraction of elements 
	 * 		in common between two sets of elements
	 * 
	 *  	@param h1 : first set of elements
	 *  	@param h2 : second set of elements
	 *  
	 *  	@return the element intersection
	 * 
 	 **/
	public static double maxFraction (ArrayList<String> h1, ArrayList<String> h2 )
	{
		
		int match = 0;
		if(!h1.isEmpty() && !h2.isEmpty()  )
		{
		for(String s1: h1)
		{
			for(String s2: h2)
			{
				if(s1.equals(s2))
					match++;
			}
		}
		
		return (double)match/(Math.max((double)h1.size(),(double)h2.size()));
		}
		else return 0.0;
	}
	
	
	
	/** calculate cosineSimilarity
	 * 
	 * @param vector a table that consist on two weighted vectors
	 * @return the similarity between these two vectors 
	 * 	       calculated using the cosine similarity
	 */
	public static float cosineQParts (Integer[][] vector)
	{
		int norm1=0, norm2=0, sum = 0;
		float similarity;
		for (int i = 0; i < vector[0].length; i++) {
		norm1 += Math.pow(vector[0][i], 2);
		norm2 += Math.pow(vector[1][i], 2);
		sum += vector[0][i] * vector[1][i];
	}
		
	
		if (norm1 == 0 && norm2 == 0)
			return 1;
		else if(norm1 == 0 || norm2 == 0)
		return 0;
	else{
		similarity = (float) sum/ (float) ((Math.sqrt(norm1) * Math.sqrt(norm2)));
		return similarity;
	}
	}
	
	
	/**This functions transforms a vector of Strings to an array of Strings
	 * 
	 * @param vec
	 * @return an array transformed from a vector
	 */
	public static ArrayList<String> VectorToArray (String[] vec)
	{
		ArrayList<String> arr = new ArrayList<String>();
		for(int i = 0; i<vec.length; i++)
			arr.add(vec[i]);
		
		
		return arr;
	}
	
	
	
	
	/**
	 * Print the matrix of dissimilarity calculated by {@code calculMatriceSelectedDocs or calculateMatrice}
	 * @param fileName : the file where the matrix will be written
	 * @param matrix   : the matrix of dissimilarity
	 */
	public static void writeCsvFile(String fileName ,float[][] matrix) {
		
		 String COMMA_DELIMITER = ";";
		 String NEW_LINE_SEPARATOR = "\n";
		 String FILE_HEADER = "contexts";
		 int length = matrix.length;
		
		 for (int i = 0; i< featureDimensions.length; i++)
		 {
			 FILE_HEADER = FILE_HEADER+";"+featureDimensions[i].toString();
		 }
		 FILE_HEADER = FILE_HEADER+"; category";
		 System.out.println(FILE_HEADER);
		 try {
			        FileWriter fileWriter = new FileWriter(fileName);
			        fileWriter.append(FILE_HEADER.toString());
			        fileWriter.append(NEW_LINE_SEPARATOR);
			        for (int i = 0; i<length; i++ )
			        {
			        	fileWriter.append(combContext[i]);
			        	fileWriter.append(COMMA_DELIMITER);
			        	for (int j=0; j<featureDimensions.length+1; j++)
			        	{
			        		fileWriter.append(String.valueOf(matrix[i][j]));
			        		if(j!=featureDimensions.length)
			        		fileWriter.append(COMMA_DELIMITER);
			        	}
			        	fileWriter.append(NEW_LINE_SEPARATOR);
			        }
			                 
			         fileWriter.flush();
	                 fileWriter.close();
			
            System.out.println("CSV file was created successfully !!!");
			         } catch (Exception e) {
			             System.out.println("Error in CsvFileWriter !!!");
			            e.printStackTrace();
			         } finally {
			           
			         }
	}
	
	

	
	
	/******* Calculates the final similarity between contexts ***********/
	public static Double[][] toClustering (Context [] c, Double[][] result)
	{
 	Double[][] matrix = new Double[c.length][c.length]; 
	int l = c.length;
    
	double distance;
	Double[] weights = { -0.0792816 ,  0.09790013 , 0.11936337 , 0.09613856 ,-0.14557173 , 0.11007091,
			   0.40146929 , 0.03131433 ,-0.11676511 };
	int m = 0;
	
 	for (int i = 0; i<l-1; i++ )
	{
		
 		matrix[i][i] = 0.0;
		for (int j = i+1; j<l; j++)
		{
		 
			distance = 0.0;
			double sum = 0.0;
			  
			for (int k = 0; k<result[i].length-1; k++)
			{
			  
				distance += result[m][k]*weights[k];
				sum+=weights[k];
			}
		//	distance = 1 - distance/featureDimensions.length;
			distance = 1 - distance/sum;
			// similarityr
			//distance = distance/featureDimensions.length;
			
			m++;
			matrix[i][j]= distance;
			matrix[j][i]= distance;
		}
	}
	matrix[l-1][l-1]=0.0;
	
	return matrix;
	}
	 
	public static void ContextsDissimilarity(Double [][] matrix, String filename)
	{
		 String COMMA_DELIMITER = ";";
		 String NEW_LINE_SEPARATOR = "\n";
		// String FILE_HEADER = "contexts";
		 int length = matrix[0].length;
		
		 for (int i = 0; i<length; i++)
		 {
		//	 FILE_HEADER = FILE_HEADER+"; C"+(i+1);
		//	 System.out.println(FILE_HEADER);
		 }
		 try {
			        FileWriter fileWriter = new FileWriter(filename);
			//        fileWriter.append(FILE_HEADER.toString());
			        fileWriter.append(NEW_LINE_SEPARATOR);
			        for (int i = 0; i<length; i++ )
			        {
			     //   	fileWriter.append("C"+(i+1));
			        	fileWriter.append(COMMA_DELIMITER);
			        	for (int j=0; j<length; j++)
			        	{
			                
			      			fileWriter.append(String.valueOf(matrix[i][j]));
			        		fileWriter.append(COMMA_DELIMITER);
			        	}
			        	fileWriter.append(NEW_LINE_SEPARATOR);
			        }
			                 
			         fileWriter.flush();
	                 fileWriter.close();
			
           System.out.println("CSV file was created successfully !!!");
			         } catch (Exception e) {
			             System.out.println("Error in CsvFileWriter !!!");
			            e.printStackTrace();
			         } finally {
			           
			         }
	}
	
	
	private static double intersectAnalysisSession (Context c1, Context c2)
	{
		ArrayList<String> analysis1 = new ArrayList<String>();
		for(Event obs : c1.getevents())
		{
			analysis1.add(obs.getSessionID());
		}
		ArrayList<String> analysis2 = new ArrayList<String>();
		for(Event obs : c2.getevents())
		{
			analysis2.add(obs.getSessionID());
		}
		
		ArrayList<String> analysis = new ArrayList<String>();
		analysis.addAll(analysis1);
		analysis.removeAll(analysis2);
		int size = analysis1.size()-analysis.size();
		double result = (double)size/Math.min((double)analysis1.size(), (double)analysis2.size());
		return result;
	}
	
		
	
	/** This feature calculates the similarity between two documents 
	 *  by searching blocks in common (shared elements).
	 *  
	 *  @param c1 first document
	 *  @param c2 second document
	 *  @return similarity score between the documents looking to the same 
	 *  		blocks shared by the two documents.
	 */
	private static float sameBlocks (Document document1, Document document2)
	{
		
		float result = (float)0.0;
		ArrayList<WebIBlock> wb1 = new ArrayList<WebIBlock>(), wb2= new ArrayList<WebIBlock>();
		for(Report rep : document1.getReports())
			wb1.addAll(rep.getBlocks());
		for(Report rep : document2.getReports())
			wb2.addAll(rep.getBlocks());
		
		int count = 0;
		for(WebIBlock block1 :wb1)
		{
			for(WebIBlock block2:wb2 )
			{
				if(block1!=null && block2!=null)
				{
				double meas = maxFraction (block1.getMeasures(), block2.getMeasures());
				double dim = maxFraction (block1.getDimensions(), block2.getDimensions());
				
				if(meas==1 && dim==1 && block1.getBlockviz().equals(block2.getBlockviz()))
					count++;
				}
			}
		}
		
		result = (float) count / (float)Math.max(wb1.size(),wb2.size());
		return result;
	}

	
		static double compareContextes (Document c1, Document c2, String usergroups, ArrayList<String[]> position, HashMap<String, Float> popularity,  HashMap<String, Integer> maxRank, HashMap<HierarchicalLevel, Integer> npath, HashMap<String, ArrayList<String>> titles ) throws FileNotFoundException, IOException, SQLException
		{
			
			
					double weights [] = {0.75187782 , 0.26421008 , 0.71039214, -0.34686768, -0.39030578

};
					double sum = 0.0;
					for (int f = 0; f < featureDimensions.length; f++) {
						//System.out.print(combContext[pos]+ "     ");
						switch (featureDimensions[f]) {
						case "group":
							sum+= sameGroup(c1,c2, usergroups)*weights[0];
							break;
						case "user":
							sum+= sameUsers(c1,c2)*weights[1];
							break;
						case "docobject" :
							sum+= (double) sameObjects(c1,c2)*weights[2];
							break;
						case "measure" :
							sum +=  sameMeasOrDim(c1,c2, "Measure")*weights[1];
						break;
						case "dimension" :
							sum +=  sameMeasOrDim(c1,c2, "Dimension")*weights[1];
						break;
						case "filter" :
							sum +=  sameFilter(c1,c2)*weights[2];
						break;
						case "event":
							sum+= sameEvent(c1,c2)*weights[2];
						break;
						case "eventtype":
							sum+= sameEventType(c1,c2)*weights[0];
							break;
						case "folder":
							sum+= sameTopFolders(c1,c2)*weights[1];
							break;
						case "universe":
							sum+= sameUniverses(c1,c2)*weights[2];
							break;
						case "session":
							sum+= sameSession(c1,c2)*weights[3];
						break;
						case "viz":
							sum += (float) sameViz(c1,c2)*weights[2];
						break;
						case "titlekeywords" :
							sum +=  sameTitleKeywords(c1,c2, titles)*weights[3];
						break;
						case "positionSession":
							sum+= samePositionInSession(c1, c2, position)*weights[4];
						break;
						case "filtervalues":
							sum += (float) sameFilterValues(c1,c2)*weights[4];
						break;
	/*					case "period":
							sum+= samePeriod(c1, c2)*weights[3];
						break;
						
						case "popularity":
							sum+= samePopularity2(c1, c2, popularity)*weights[5];
						break;
						case "dimensions":
							sum += (float) sameObjectsWithoutMeasures(c1, c2)*weights[1];
						break;
						
						case "blocks":
							sum += (float) sameBlocks(c1,c2)*weights[1];
						break;
//						case "prompttype":
//						sum+= sameUniverseObjects(c1,c2, "Prompt Name")*weights[6];
//     					break;
//						case "promptvalue" : 
//						sum+= sameUniverseObjects (c1,c2, "Prompt Value")*weights[7];
//						break;
					
			*/
						default:
							System.out.println("No feature dimension !");
						}					
					}
			//		System.out.println((double)sum/(double)featureDimensions.length);
					return ((double)sum/(double)featureDimensions.length);//
				//	return (1-((double)sum/(double)featureDimensions.length));
					
		}
			
	
		 static double compareSimilaritiesNbObservations(String file1, String file2)
		{
			double res = 0.0;
			
			 String COMMA_DELIMITER = ";";
			 String NEW_LINE_SEPARATOR = "\n";
			 String line = "", line2 = "";
			 int nr = 0;
			 
		
				        BufferedReader delim1 = null , delim2 = null;

				        try {

				            delim1 = new BufferedReader(new FileReader(file1));
				            delim2 = new BufferedReader(new FileReader(file2));
				            while ((line = delim1.readLine()) != null) {
				                 
				            	line2 = delim2.readLine();
				                // use comma as separator
				                String[] values1 = line.split(COMMA_DELIMITER);
				                String[] values2 = line2.split(COMMA_DELIMITER);

				                for(int i = 0; i<95; i++)
				                	if(Double.parseDouble(values1[i]) > Double.parseDouble(values2[i]))
				                		nr++;

				            }

				        } catch (FileNotFoundException e) {
				            e.printStackTrace();
				        } catch (IOException e) {
				            e.printStackTrace();
				        } finally {
				            if (delim1 != null) {
				                try {
				                    delim1.close();
				                    delim2.close();
				                } catch (IOException e) {
				                    e.printStackTrace();
				                }
				            }
				        }
				        
				        res =  (double)nr /(double)(95*95);
				        return res;
		}
		
			

		 
		 public static void writeCsvFileJulien(String fileName ,Double[][] result) {
				
			 String COMMA_DELIMITER = ";";
			 String NEW_LINE_SEPARATOR = "\n";
			 String FILE_HEADER = "contexts";
			 int length = result.length;
			
			 
			 FILE_HEADER = FILE_HEADER+"; category";
			 System.out.println(FILE_HEADER);
			 try {
				        FileWriter fileWriter = new FileWriter(fileName);
				        fileWriter.append(FILE_HEADER.toString());
				        fileWriter.append(NEW_LINE_SEPARATOR);
				        for (int i = 0; i<length; i++ )
				        {
				        	fileWriter.append(combContext[i]);
				        	fileWriter.append(COMMA_DELIMITER);
				        	for (int j=0; j<2; j++)
				        	{
				        		fileWriter.append(String.valueOf(result[i][j]));
				        		fileWriter.append(COMMA_DELIMITER);
				        	}
				        	fileWriter.append(NEW_LINE_SEPARATOR);
				        }
				                 
				         fileWriter.flush();
		                 fileWriter.close();
				
	            System.out.println("CSV file was created successfully !!!");
				         } catch (Exception e) {
				             System.out.println("Error in CsvFileWriter !!!");
				            e.printStackTrace();
				         } finally {
				           
				         }
		}



		public static double findSimilarityInFile(String docID, String docID2, String usergroups, String path, Document doc1, Document doc2,  ArrayList<String[]> position, HashMap<String, Float> popularity, HashMap<String, Integer> maxRank, HashMap<HierarchicalLevel, Integer> npath, HashMap<String, ArrayList<String>> titles) throws IOException, SQLException {

			double res = 0.0;
			FileReader file = new FileReader(path);
			BufferedReader br = new BufferedReader(file);
			
			String newline =  br.readLine();
			String[] docsID =newline.split(" ; ");
			br.close();
			ArrayList<String> docs = VectorToArray(docsID);
			
			int d1 = docs.indexOf(docID);
			int d2 = docs.indexOf(docID2);
			
			String lineString="no new line";
			try (Stream<String> lines = Files.lines(Paths.get(path))) {
			    lineString = lines.skip(Math.min(d1, d2)+1).findFirst().get();
			   // lineString = lines.skip(4).findFirst().get();
			}
			System.out.println(lineString);
		
			String[] line = lineString.split(";");
			String similar = line[Math.max(d1, d2)];
			res = Double.parseDouble(similar);
			
			String connection = "jdbc:postgresql://[::1]:5432/audit1503";
			String user = "postgres";
			String pass = "Password1";
	        Connection conHana = DriverManager.getConnection(connection, user, pass);
//			Document doc1 = Document.createDocByID(conHana, docID);
//			Document doc2 = Document.createDocByID(conHana, docID2);
	        conHana.close();
			double res2 = compareContextes(doc1, doc2, usergroups, position, popularity, maxRank,  npath, titles);
			return res;
		}



		public static double findSimilarity(String docID, String docID2, double[][]simItems, String[]docIDs) {
		
			double res = 0.0;
			int posItem1 = indexOf(docIDs, docID);
			int posItem2 = indexOf(docIDs, docID2);
			if(posItem1<posItem2)
				return simItems[posItem1][posItem2];
			else return simItems[posItem2][posItem1];
			
			
		}
		
		private static int indexOf(Object[] users, Object user) {
			for(int i = 0; i<users.length; i++)
			{
				if(users[i].equals(user))
					return i;
			}
			return -1;
		}
		

		/** Find the position of max value of a vector
		 * */
		private static int maxVector(Integer[] posvec1) {
		   int max = -1;
		   int pos = 0;
			for (int i=0; i<posvec1.length; i++)
			{
				if(posvec1[i]>max)
				{
					max = posvec1[i];
					pos= i;
				}
			}
			return pos;
		}


		private static float sameFilterValues(Document document, Document document2) {
			
			float result=(float) 0.0;
		
			
			HashMap<String, ArrayList<String>> filters1 = document.getFilters(), filters2 = document2.getFilters();
			
			if(filters1==null || filters2==null)
				return (float)0.0;
			if(!filters1.isEmpty())
			{
			Iterator it = filters1.entrySet().iterator();
			while(it.hasNext())
				{
				 Entry<String, ArrayList<String>> pair = (Entry) it.next();
				// System.out.println("\n Filters1: "+pair.toString()+"\n");
				 
				 Iterator it2 = filters2.entrySet().iterator();
				 while(it2.hasNext())
					 {
					 	Entry<String, ArrayList<String>> pair2 = (Entry<String, ArrayList<String>>) it2.next();
					//	 System.out.println("Filters2: "+pair2.toString()+"\n");
					 	
					 	ArrayList<String> simval = getSimilarValues(pair.getValue(), pair2.getValue());
				//	 	if(!simval.isEmpty())
					 	if(pair.getKey().equals(pair2.getKey()))
					 		{
					 		if(pair.getValue().get(0).equals(pair2.getValue().get(0)) // if they both use the same operator
					 				|| (pair.getValue().get(0).contains("GREATER") && pair2.getValue().get(0).contains("GREATER"))
					 					|| (pair.getValue().get(0).contains("LESS") && pair2.getValue().get(0).contains("LESS"))) // GREATER/LESS same with GREATER/LESS OREQUAL
					 		{
					 			if(pair.getValue().get(0).contains("EQUAL") || pair.getValue().get(0).equals("GREATER") || pair.getValue().get(0).equals("LESS") || pair.getValue().get(0).contains("NULL")) // includes NOTEQUAL, GREATEROREQUAL, LESSOREQUAL, NOTNULL
					 				result++;
					 			else if (pair.getValue().get(0).contains("INLIST")) // includes NOTINLIST
					 				result += (float) simval.size() / (float) Math.max(pair.getValue().size()-1, pair2.getValue().size()-1);
					 			else if (pair.getValue().get(0).equals("BETWEEN"))
					 			{
					 				if(simval.size()==2)	result++;
					 				else {
					 					float diff = (float) 0.0;
					 					if(pair.getKey().contains("eek"))
					 					{
					 						String week11 = pair.getValue().get(3).substring(pair.getValue().get(3).indexOf("-")+2,pair.getValue().get(3).length());
					 						String week21 = pair2.getValue().get(3).substring(pair2.getValue().get(3).indexOf("-")+2,pair2.getValue().get(3).length());
					 						String week12 = pair.getValue().get(6).substring(pair.getValue().get(6).indexOf("-")+2,pair.getValue().get(6).length());
					 						String week22 = pair2.getValue().get(6).substring(pair2.getValue().get(6).indexOf("-")+2,pair2.getValue().get(6).length());
					 							diff = Math.max(Math.abs(Integer.valueOf(week11)-Integer.valueOf(week21)) 
					 									, Math.abs(Integer.valueOf(week12)-Integer.valueOf(week22))); // difference between different values of BETWEEN operator
					 							
					 							result += diff / (float) Math.max(Math.abs(Integer.valueOf(week11)-Integer.valueOf(week12)), Math.abs(Integer.valueOf(week21))-Integer.valueOf(week22)); // max range of BETWEEN
					 					}
					 						else
					 						{
					 						diff = Math.max(Math.abs(Integer.valueOf(pair.getValue().get(1))-Integer.valueOf(pair2.getValue().get(1))), Math.abs(Integer.valueOf(pair.getValue().get(2))-Integer.valueOf(pair2.getValue().get(2)))); // difference between different values of BETWEEN operator
					 						result += diff / (float) Math.max(Math.abs(Integer.valueOf(pair.getValue().get(1))-Integer.valueOf(pair.getValue().get(2))), Math.abs(Integer.valueOf(pair2.getValue().get(1))-Integer.valueOf(pair2.getValue().get(2)))); // max range of BETWEEN
					 						}
					 				}
					 					
					 					
					 			}
					 		}
					 		else // if the operators are different, there are some cases to consider that interrogates close parts of the database
					 		{
					 		
					 		if((pair.getValue().get(0).equals("EQUAL") || pair.getValue().get(0).equals("INLIST")) 
					 			&& (pair2.getValue().get(0).equals("EQUAL") || pair2.getValue().get(0).equals("INLIST")))
					 			result += (float) simval.size() / (float) Math.max(pair.getValue().size()-1, pair2.getValue().size()-1);
					 		else if ((pair.getValue().get(0).equals("EQUAL") || pair.getValue().get(0).equals("GREATEROREQUAL") || pair.getValue().get(0).equals("LESSOREQUAL")) 
						 			&& (pair2.getValue().get(0).equals("EQUAL") || pair.getValue().get(0).equals("GREATEROREQUAL") || pair.getValue().get(0).equals("LESSOREQUAL")))
					 			result += 0.1;	// static value to test, to simplify the calculations and not to calculate all the possible values according to filters
					 		else if ((pair.getValue().get(0).equals("EQUAL") || pair.getValue().get(0).equals("BETWEEN"))
					 				&& (pair2.getValue().get(0).equals("EQUAL") || pair2.getValue().get(0).equals("BETWEEN")))
					 		{
					 			if(pair.getValue().size()<3) // BETWEEN is the operator of the second filter
					 			result += (float) simval.size() / (float) Math.abs(Integer.valueOf(pair2.getValue().get(1))-Integer.valueOf(pair2.getValue().get(2)));
					 			else // BETWEEN is the operator of the first filter
					 			{
					 				if(pair.getValue().get(1).matches("[0-9]+") && pair.getValue().get(2).matches("[0-9]+"))
					 				result += (float) simval.size() / (float) Math.abs(Integer.valueOf(pair.getValue().get(1))-Integer.valueOf(pair.getValue().get(2)));
					 			}
					 		}
					 		else if ((pair.getValue().get(0).equals("INLIST") || pair.getValue().get(0).contains("GREATER") || pair.getValue().get(0).contains("LESS") )   // includes GREATEROREQUAL & LESSOREQUAL
					 				&& (pair2.getValue().get(0).equals("INLIST") || pair2.getValue().get(0).contains("GREATER") || pair2.getValue().get(0).contains("LESS")))
					 		{
					 			int pos ;
					 				if(pair.getValue().size()>1) // INLIST is the operator of the first filter
					 				{
					 					Collections.sort(pair.getValue()); // order the values of the array
					 					pos = pair.getValue().indexOf(pair2.getValue().get(1)); // get the position of the common value 
					 					if(pair2.getValue().get(0).contains("OREQUAL"))
					 					result += (float) (pos+1) / (float) (pair.getValue().size());
					 					else 
					 						result += (float) pos / (float) (pair.getValue().size());
					 				}
						 			else // INLIST is the operator of the second filter
						 			{
						 				Collections.sort(pair2.getValue()); // order the values of the array
					 					pos = pair2.getValue().indexOf(pair.getValue().get(1)); // get the position of the common value 
					 					if(pair.getValue().get(0).contains("OREQUAL"))
					 					result += (float) (pos+1) / (float) (pair2.getValue().size());
					 					else 
					 						result += (float) pos / (float) (pair2.getValue().size());
						 			}
					 		}
					 		else if ((pair.getValue().get(0).equals("INLIST") || pair.getValue().get(0).equals("BETWEEN"))
					 				&& (pair2.getValue().get(0).equals("INLIST") || pair2.getValue().get(0).equals("BETWEEN")))
					 		{
					 			int dist1, dist2;
					 			if(pair.getValue().size()>1) // INLIST is the operator of the first filter
				 				{
				 					Collections.sort(pair.getValue()); // order the values of the array
				 					dist1 = (Integer.valueOf(pair2.getValue().get(1))-Integer.valueOf(pair.getValue().get(1))>0) ? (Integer.valueOf(pair2.getValue().get(1))-Integer.valueOf(pair.getValue().get(1))) : 0;
				 					dist2 = (Integer.valueOf(pair2.getValue().get(1))-Integer.valueOf(pair.getValue().get(1))>0) ? (Integer.valueOf(pair2.getValue().get(1))-Integer.valueOf(pair.getValue().get(1))) : 0;
				 					result += 1-((float) (dist1+dist2) / (float) (pair.getValue().size()));
				 				}
					 			else // INLIST is the operator of the second filter
					 			{
					 				Collections.sort(pair2.getValue()); // order the values of the array
					 				dist1 = (Integer.valueOf(pair.getValue().get(1))-Integer.valueOf(pair2.getValue().get(1))>0) ? (Integer.valueOf(pair.getValue().get(1))-Integer.valueOf(pair2.getValue().get(1))) : 0;
				 					dist2 = (Integer.valueOf(pair.getValue().get(1))-Integer.valueOf(pair2.getValue().get(1))>0) ? (Integer.valueOf(pair.getValue().get(1))-Integer.valueOf(pair2.getValue().get(1))) : 0;
				 					result += 1-((float) (dist1+dist2) / (float) (pair2.getValue().size()));
					 			}
					 		}
					 		else if ((pair.getValue().get(0).equals("BETWEEN") || pair.getValue().get(0).contains("GREATER"))
					 				&& (pair2.getValue().get(0).equals("BETWEEN") || pair2.getValue().get(0).contains("GREATER")))
					 		{
					 			if(pair.getValue().size()>1) // BETWEEN is the operator of the first filter
				 				{
					 				if(Integer.valueOf(pair2.getValue().get(1))<Integer.valueOf(pair.getValue().get(1))) 
					 					result +=1;
					 				else if (Integer.valueOf(pair2.getValue().get(1))<Integer.valueOf(pair.getValue().get(2)))
					 					result += Integer.valueOf(pair.getValue().get(2))-Integer.valueOf(pair2.getValue().get(1)) 
					 								/ (float)(Integer.valueOf(Integer.valueOf(pair.getValue().get(2))-Integer.valueOf(Integer.valueOf(pair.getValue().get(1)))));
				 				}
					 			else // BETWEEN is the operator of the second filter
					 			{
					 				if(Integer.valueOf(pair.getValue().get(1))<Integer.valueOf(pair2.getValue().get(1))) 
					 					result +=1;
					 				else if (Integer.valueOf(pair.getValue().get(1))<Integer.valueOf(pair2.getValue().get(2)))
					 					result += Integer.valueOf(pair2.getValue().get(2))-Integer.valueOf(pair.getValue().get(1)) 
					 								/ (float)(Integer.valueOf(Integer.valueOf(pair2.getValue().get(2))-Integer.valueOf(Integer.valueOf(pair2.getValue().get(1)))));
					 			}				 		
					 			}
					 		else if ((pair.getValue().get(0).equals("BETWEEN") || pair.getValue().get(0).contains("LESS"))
					 				&& (pair2.getValue().get(0).equals("BETWEEN") || pair2.getValue().get(0).contains("LESS")))
					 		{
					 			if(pair.getValue().size()>1) // BETWEEN is the operator of the first filter
				 				{
					 				if(Integer.valueOf(pair2.getValue().get(1))>Integer.valueOf(pair.getValue().get(2))) 
					 					result +=1;
					 				else if (Integer.valueOf(pair2.getValue().get(1))>Integer.valueOf(pair.getValue().get(1)))
					 					result += Integer.valueOf(pair2.getValue().get(1))-Integer.valueOf(pair.getValue().get(1)) 
					 								/ (float)(Integer.valueOf(Integer.valueOf(pair.getValue().get(2))-Integer.valueOf(Integer.valueOf(pair.getValue().get(1)))));
				 				}
					 			else // BETWEEN is the operator of the second filter
					 			{
					 				if(Integer.valueOf(pair.getValue().get(1))>Integer.valueOf(pair2.getValue().get(2))) 
					 					result +=1;
					 				else if (Integer.valueOf(pair.getValue().get(1))>Integer.valueOf(pair2.getValue().get(1)))
					 					result += Integer.valueOf(pair.getValue().get(1))-Integer.valueOf(pair2.getValue().get(1)) 
					 								/ (float)(Integer.valueOf(Integer.valueOf(pair2.getValue().get(2))-Integer.valueOf(Integer.valueOf(pair2.getValue().get(1)))));
					 			}				 		
					 			}				 			
					 		
					 	 
					 		}
					 			
					 }
					 }
				
				}
				}
			
		return result;
	}



		public static ArrayList<String> getSimilarValues(ArrayList<String> pair,ArrayList<String> pair2) {
			
			ArrayList<String> result = new ArrayList<String>();
			
			
			for(int i=1; i<pair.size();i++)
			{
				if (pair2.contains(pair.get(i)))
				{
					result.add(pair.get(i));
					
				}
			}
			
			return result;
		}



		private String[][] rankUsers(HashMap<String, Integer> freqUser1,
				HashMap<String, Integer> freqUser2) {
			
			String[][] result = new String[2][Math.max(freqUser1.size(), freqUser2.size())];
			
			int max = 0;
			int freq1pos = 0;
			String idMax = "";
			while(freqUser1.size()>0)
			{
				max = 0;
				for(Map.Entry<String, Integer> entry : freqUser1.entrySet())
				{
					if(entry.getValue()>max)
					{
						max = entry.getValue();
						idMax = entry.getKey();
					}
				
				}
				result[0][freq1pos] = idMax;
				freqUser1.remove(idMax);
				freq1pos++;  
			}
			
			freq1pos = 0;
			while(freqUser2.size()>0)
			{
				max = 0;
				for(Map.Entry<String, Integer> entry : freqUser2.entrySet())
				{
					if(entry.getValue()>max)
					{
						max = entry.getValue();
						idMax = entry.getKey();
					}
				
				}
				result[1][freq1pos] = idMax;
				freqUser2.remove(idMax);
				freq1pos++;  
			}
			
			return result;
		}



		private static float samePopularity(Document document, Document document2, HashMap<String, Integer> popularity) {
			float res = 0;
			int pop1, pop2;
			if(popularity.get(document.getDocID())==null || popularity.get(document2.getDocID())==null)
				return (float) 0.0;
			else
			{
			 pop1 = popularity.get(document.getDocID());
			 pop2 = popularity.get(document2.getDocID());
			}
			
			return (float)Math.abs(pop1-pop2)/ (float) Math.max(pop1, pop2);
		}


		private static float samePopularity2(Document document, Document document2, HashMap<String, Float> popularity) {
			float res = 0;
			Float pop1;
			Float pop2;
			if(popularity.get(document.getDocID())==null || popularity.get(document2.getDocID())==null)
				return (float) 0.0;
			else
			{
			 pop1 = popularity.get(document.getDocID());
			 pop2 = popularity.get(document2.getDocID());
			}
			
			return (float)Math.abs(pop1-pop2);
		}

		private static float samePositionInSession(Document document, Document document2, ArrayList<String[]> position) {
			
			Integer[] posvec1 = {0,0,0};
			Integer[] posvec2 = {0,0,0};
			int pos1, pos2;
			
			for(String[] vec : position)
			{
				int p = Integer.parseInt(vec[2]);
				if(vec[1].equals(document.getDocID()))
					posvec1[p-1] += Integer.parseInt(vec[3]);
				else if (vec[1].equals(document2.getDocID()))
					posvec2[p-1] += Integer.parseInt(vec[3]);
			}
			
			pos1 = maxVector(posvec1);
			pos2 = maxVector(posvec2);
			
			if(pos1==pos2)
				return 1;
			return 0;
		}



		public static HashMap<String, String> updateLastVisited( Document[] contexts) throws SQLException {
			
			HashMap<String, String> lastUP = new HashMap<String, String>();
			
		
			
			for (Document d : contexts)
			{
			ArrayList<String> datetimes = d.getTimestamp();
			String lastUpdateDate = datetimes.get(0).replaceAll("\\s{2,}", " ").trim();
			int nr = 1;
			
			while(nr<datetimes.size())
				{
					String[] parts = lastUpdateDate.split(" ");
					DateTime lastTime = new DateTime(parts[0], Integer.parseInt(parts[1]), Integer.parseInt(parts[2]), parts[3]);
					String completeDate = datetimes.get(nr).replaceAll("\\s+", " ");
					parts = completeDate.split(" ");
					DateTime datetime = new DateTime(parts[0], Integer.parseInt(parts[1]), Integer.parseInt(parts[2]), parts[3]);
					if(datetime.year>lastTime.year)
						lastUpdateDate = completeDate;
					else if (datetime.year==lastTime.year && datetime.month > lastTime.month)
						lastUpdateDate = completeDate;
					else if (datetime.year==lastTime.year && datetime.month==lastTime.month && datetime.date>lastTime.date)
						lastUpdateDate = completeDate;
					else if (datetime.year==lastTime.year && datetime.month==lastTime.month && datetime.date==lastTime.date && datetime.ampm>datetime.ampm)
						lastUpdateDate = completeDate;
					nr++;
				}
			
			lastUP.put( d.getDocID(), lastUpdateDate);
		
			}
			
			return lastUP;
		}



		private static float samePeriod(Document document1, Document document2) {
			
			float sim = 0;
			
			String[] date1 = lastUpdate.get(document1.getDocID()).split(" ");
			String[] date2 = lastUpdate.get(document2.getDocID()).split(" ");
			DateTime update1 = new DateTime(date1[0], Integer.parseInt(date1[1]), Integer.parseInt(date1[2]), date1[3]);
			DateTime update2 = new DateTime(date2[0], Integer.parseInt(date2[1]), Integer.parseInt(date2[2]), date2[3]);
			
			float simUpdate = 0;
			
			if(Math.abs(update1.year-update2.year)>1 || (Math.abs(update1.year-update2.year)==1 && DateTime.getDiffMonth(update1, update2)>6))
				simUpdate = (float) 1.0;
			else if (Math.abs(update1.month-update2.month)>1 || (Math.abs(update1.month-update2.month)==1 && DateTime.getDiffDate(update1, update2)>15) )
				simUpdate = (float) Math.sqrt(Math.sqrt(DateTime.getDiffMonth(update1, update2))) * (float)0.5;
			else if (Math.abs(update1.date-update2.date)>1 || (Math.abs(update1.date-update2.date)==1 && DateTime.getDiffTime(update1, update2)>1200))
				simUpdate = (float)Math.sqrt(Math.sqrt(DateTime.getDiffDate(update1, update2))) * (float)0.2;
			else
				simUpdate = (float)DateTime.getDiffTime(update1, update2) * (float)0.0002;
			
			sim = 1-simUpdate;
			
			return sim;
		}
		
		public static double sameObjectsWithoutMeasures(Document c1, Document c2 ) throws IOException {
			
			ArrayList<String> dimensions1 =  c1.getDocDimensions();
			ArrayList<String> dimensions2 = c2.getDocDimensions();
			
			dimensions1.remove("<null>");
			dimensions2.remove("<null>");
			return maxFraction(dimensions1, dimensions2);
			
			
		}
		
		//This feature calculates the similarity between two contexts in terms of the levels of objects used in documents used by their events
	/*		public static float sameObjectLevels(Document c1, Document c2, HashMap<String, Integer> maxRank, HashMap<HierarchicalLevel, Integer> npath  ) throws IOException {
				
				ArrayList<String> objects1 =  c1.getDocDimensions();
				ArrayList<String> objects2 = c2.getDocDimensions();
				ArrayList<String> filters1 = new ArrayList<String>();
				ArrayList<String> filters2 = new ArrayList<String>();
				
				Set<String> f1 = c1.getFilters().keySet();
				Set<String> f2 = c2.getFilters().keySet();
				
				if(!f1.isEmpty())
				{
				for(String f:f1)
					filters1.add(f);
				objects1.addAll(f1);
				}
				if(!f2.isEmpty())
				{
				for(String f:f2)
					filters2.add(f);
				objects2.addAll(f2);
			}
				
				
				
				ArrayList<String> universe1 = new ArrayList<String>();
				ArrayList<String> universe2 = new ArrayList<String>();
				float level = 0;
				for(String o1: objects1)
				{
					universe1 = getUniverseForObject(npath, o1);
					for(String o2: objects2)
					{
						universe2 = getUniverseForObject(npath,o2);
						ArrayList<String> intersec = new ArrayList<String>();
						intersec.addAll(universe1);
						intersec.retainAll(universe2);
						
						if(!intersec.isEmpty())
						{
						for(String u:intersec)
						{
							String path1 = getNavPath(npath, u, o1);
							String path2 = getNavPath(npath, u, o2);
							if(path1.equals(path2))
							{
								HierarchicalLevel h1 = new HierarchicalLevel(u, path1, o1);
								HierarchicalLevel h2 = new HierarchicalLevel(u, path1, o2);
								Integer l1 = null, l2 = null;
								
								Iterator it = npath.entrySet().iterator();
								while(it.hasNext())
								{
									Entry entry = (Entry) it.next();
									HierarchicalLevel hl1 = (HierarchicalLevel) entry.getKey();
									if(hl1.getUniverse().equals(h1.getUniverse()) && hl1.getNavPath().equals(h1.getNavPath()) && hl1.getObject().equals(h1.getObject()))
										l1=(Integer) entry.getValue();
									if(hl1.getUniverse().equals(h2.getUniverse()) && hl1.getNavPath().equals(h2.getNavPath()) && hl1.getObject().equals(h2.getObject()))
										l2=(Integer) entry.getValue();
								}
								level+= 1-((float)Math.abs(l1 - l2) / (float)maxRank.get(path1));
							}
							
						}
						}
						
					}
				}
			
				if(objects1.size()==0 || objects2.size()==0)
					return 0;
				return  level/(objects1.size()*objects2.size());
			}
			
			*/
			
			/** Find the universes that the object is part of
			 * */
	/*		static ArrayList<String> getUniverseForObject(HashMap<HierarchicalLevel, Integer> npath , String object)
			{
				ArrayList<String> res = new ArrayList<String>();
				
				Iterator it = npath.entrySet().iterator();
				while(it.hasNext())
				{
					Entry entry = (Entry) it.next();
					HierarchicalLevel hl = (HierarchicalLevel) entry.getKey();
					
					if(hl.getObject().equals(object))
						res.add(hl.getUniverse());
				}
					
				return res;
			}
		*/
			/** Finds the navigational path of an object in a universe
			 * */
		/*	static String getNavPath (HashMap<HierarchicalLevel, Integer> npath,String universe, String object)
			{
				Iterator it = npath.entrySet().iterator();
				while(it.hasNext())
				{
					Entry entry = (Entry) it.next();
					HierarchicalLevel hl = (HierarchicalLevel) entry.getKey();
					if(hl.getUniverse().equals(universe) && hl.getObject().equals(object))
						return hl.getNavPath();
				}
				return null;
					
			
			}
			*/
		
		//fraction of events differs by only one BIObject
		/*public static float OneBIObject(Context c1, Context c2)
		{
			float res = 0;
			int count = 0;
			ArrayList<String> objects1 = new ArrayList<String>();
			ArrayList<String> objects2 = new ArrayList<String>();
			
			for(Event o : c1.getevents())
			{
				objects1 = new ArrayList<String>();
				for (String m : o.getDocMeasures())
					objects1.add(m);
				
				for (String db : o.getDocDimensions())
					objects1.add(db);
				
				if(o.getFilters()!=null && !o.getFilters().isEmpty())
				{
				Iterator it = (Iterator) o.getFilters().entrySet();
				while(it.hasNext())
				{
					Map.Entry pair = (Entry) it.next();
					objects1.add((String) pair.getKey());
				}
				}
				
				for (Event o2 : c2.getevents())
				{
					objects2 = new ArrayList<String>();
					for (String m : o2.getDocMeasures())
						objects2.add(m);
					
					for (String db : o2.getDocDimensions())
						objects2.add(db);
					if(o2.getFilters()!=null && !o2.getFilters().isEmpty())
					{
					Iterator it2 = (Iterator) o2.getFilters().entrySet();
					while(it2.hasNext())
					{
						Map.Entry pair = (Entry) it2.next();
						objects2.add((String) pair.getKey());
					}
					}
					
					if(Math.abs(objects1.size()-objects2.size()) < 2)
					{
					
					ArrayList<String> copy1 = new ArrayList<String>();
					copy1.addAll(objects1);
					copy1.removeAll(objects2);
					objects2.removeAll(objects1);
					if(copy1.size() == 1 || objects2.size() == 1)
						count++;
					}
				}
				
			}
			
			res = (float) count/ (float)( c1.getevents().size()*c2.getevents().size());
			
			return res;
		}
		*/
		// Values of filters and prompts lead in a way the content of the documents, so I consider this as "answer" 
		/*public static float sameAnswer(Context c1, Context c2)
		{
			float res=0;
			List<String> answer1, answer2, element;
			ArrayList<String> groupAnswers1 = new ArrayList<String>(), groupAnswers2= new ArrayList<String>();
			HashMap<String, Integer> objects1 = new HashMap<String, Integer>(), objects2= new HashMap<String, Integer>();
		    int lines1=0,lines2=0, size1=0, size2=0, count=0;
		    
		    for(Event obs1: c1.getevents())
		    {
		    	if(obs1.getFilters()!=null && !obs1.getFilters().isEmpty())
		    	{
		    	Iterator itFilter = (Iterator) obs1.getFilters().entrySet();
		    	while(itFilter.hasNext())
		    	{
		    	   Map.Entry pair = (Entry) itFilter.next();
		    	   groupAnswers1.addAll((ArrayList<String>)pair.getValue());
		    	}
		    	}
		    	if(obs1.getEventDetails()!=null && !obs1.getEventDetails().isEmpty())
		    	{
		    	Iterator itPrompt = (Iterator) obs1.getEventDetails().entrySet();
		    	while(itPrompt.hasNext())
		    	{
		    	   Map.Entry pair = (Entry) itPrompt.next();
		    	   groupAnswers1.addAll((ArrayList<String>)pair.getValue());
		    	}
		    	
		    	objects1 = getFrequences(objects1, groupAnswers1);
		    }
		    }
		    	
		    for(Event obs2: c2.getevents())
		    	{
		    	if(obs2.getFilters()!=null && !obs2.getFilters().isEmpty())
		    	{
		    		Iterator itFilter2 = (Iterator) obs2.getFilters().entrySet();
			    	while(itFilter2.hasNext())
			    	{
			    	   Map.Entry pair = (Entry) itFilter2.next();
			    	   groupAnswers2.addAll((ArrayList<String>)pair.getValue());
			    	}
		    	}
		    	    if(obs2.getEventDetails()!=null && !obs2.getEventDetails().isEmpty())
		    	    {
			    	Iterator itPrompt2 = (Iterator) obs2.getEventDetails().entrySet();
			    	while(itPrompt2.hasNext())
			    	{
			    	   Map.Entry pair = (Entry) itPrompt2.next();
			    	   groupAnswers2.addAll((ArrayList<String>)pair.getValue());
			    	}
			    	
			    	objects2 = getFrequences(objects2, groupAnswers1);
		    	}
		    	}
		    	    
		    
		    
		    HashSet<String> Uobjects = new HashSet<String>();
		    Uobjects.addAll(objects1.keySet());
			Uobjects.addAll(objects2.keySet());
			
			Integer[][] vectors = new Integer[2][Uobjects.size()];
			Arrays.fill(vectors[0], 0);
			Arrays.fill(vectors[1], 0);
			
			int index = 0;
			for (String ob : Uobjects){
				vectors[0][index] = objects1.get(ob) != null ? objects1.get(ob) : 0;
				vectors[1][index] = objects2.get(ob) != null ? objects2.get(ob) : 0;
				index++;
			}
			
			
				return cosineQParts(vectors);
		    
		}
		*/
		
		public static float maxFractionArrays (ArrayList<String> group1, ArrayList<String> group2 )
		{
			int match = 0;
	        for(String s1: group1)
	        {
	              //ArrayList<String> groups1 = VectorToArray(s1.split(","));
	              for(String s2: group2)
	              {
	                    //ArrayList<String> groups2 = VectorToArray(s2.split(","));
	                    //groups1.retainAll(groups2);
	                    //if(!groups1.isEmpty())
	                    if(s1.equals(s2))
	                          match++;
	              }
	        }
	        //System.out.println("match :"+ match);
	        if(group1.size()==0 && group2.size()==0)
	              return (float) 1.0;
	        else if(group1.size()==0 || group2.size()==0)
	              return (float) 0.0;
	        return  (float)match/((float) group1.size()*group2.size());
	       // return ((float)match/Math.min(group1.size(), group2.size()));

		}
		
		public static HashMap<Filter, Integer> getFilterFreq(HashMap<Filter, Integer> occurences, ArrayList<Filter> entries)
		{
			if(entries!=null)
			{
			for(Filter f:entries)
			{
				String str = f.getFilterName();
				if (occurences.containsKey(str))
					occurences.put(f, occurences.get(str) + 1);
				else
					occurences.put(f, 1);
			}
			}
			return occurences;
		}
		
		/* This function merges two functions in one */
		public static Context mergeContexts(Context c1, Context c2) {

			Context c = new Context();
			Set<Event> obsTotal = new HashSet<Event>();
			ArrayList<Event> obs1 = c1.getevents();
			ArrayList<Event> obs2 = c2.getevents();
			obsTotal.addAll(obs1);
			obsTotal.addAll(obs2);

			c.getevents().addAll(obsTotal);

			return c;

		}
		
		
		private static ArrayList<String> toLowCase (ArrayList<String> str)
		{
			ArrayList<String> newString = new ArrayList<String>(); 
			for(String s : str)
			{
				newString.add(s.toLowerCase());
			}
			return newString;
		}
		
		public static int factorial(int number) {
			 BigInteger n = BigInteger.valueOf(1);
			    for (int i = 1; i <= number; i++)
			        n = n.multiply(BigInteger.valueOf(i));
			 BigInteger r = BigInteger.valueOf(1);
			 for (int i = 1; i <= number-2; i++)
			        r = r.multiply(BigInteger.valueOf(i));
			 int fact = n.divide(r.multiply(BigInteger.valueOf(2))).intValue();
			  return fact;
		}



	
}
