package structure;

import java.util.HashMap;

/** This class describes the usage of a document, the number of users and the frequency for each.
 * The reason is to compare documents based on the common users **/
public class DocUsage {
	
	private String idDoc;
	private HashMap<String, Integer> userFreq;
	
	public DocUsage()
	{
		this.idDoc = "";
		this.userFreq = new HashMap<String, Integer>();
	}
	
	public DocUsage( String id, HashMap<String,Integer> freq)
	{
		this.idDoc = id;
		this.userFreq = freq;
	}

	
	public String getIdDoc() {
		return idDoc;
	}
	public void setIdDoc(String idDoc) {
		this.idDoc = idDoc;
	}
	public HashMap<String, Integer> getUserFreq() {
		return userFreq;
	}
	public void setUserFreq(HashMap<String, Integer> userFreq) {
		this.userFreq = userFreq;
	}
	
	public void addUser (DocUsage docusage, String idUser, int freq)
	{
		if(docusage.getUserFreq().containsKey(idUser))
			docusage.getUserFreq().replace(idUser, freq);
		else
			docusage.getUserFreq().put(idUser, freq);
	}

}
