package dataStructures;
import java.util.ArrayList;
import java.util.List;


public class Filter {

	private String filterName;
	private List<String> values;
	
	public Filter()
	{
		filterName="";
		values = new ArrayList<String>();
	}
	
	public Filter(String filterName, ArrayList<String> values) {
		this.filterName = filterName;
		this.values = values;
	}
	public String getFilterName() {
		return filterName;
	}
	public void setFilterName(String filterName) {
		this.filterName = filterName;
	}
	public List<String> getValues() {
		return values;
	}
	public void setValues(List<String> values) {
		this.values = values;
	}
	
	
}
