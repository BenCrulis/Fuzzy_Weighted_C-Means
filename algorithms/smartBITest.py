"""
This class implements a test with the datasets of SmartBi experiment with the Bundle algorithm by Sihem et al.
Dataset has been preprocessed so that each query is represented in a latent space of topics computed with LDA.
"""

import pandas as pd
import numpy as np
from utility.comparison import cos
from algorithms.BundleCMeans import BundleCmeans

#filepath = "/Users/labroche/Desktop/smartbi/questions_output_detail_10.csv"
#df = pd.read_csv(filepath, sep=";",header=None)
##df.drop(range(3), axis=1, inplace=True)         # remove uninteresting columns (id, theoretical topic, topic score)
#dataset = df.as_matrix(columns=range(3,13))
#user = np.random.rand(10)
#s = np.sum(user)
#user = np.divide(user, s)
#budget = 5
#bcm = BundleCmeans(c=10, user=user, budget=budget, sim=cos, eta = 10, m=2, epsilon=10e-5, max_nb_iter=100)
#bcm.fit(dataset, alpha=0.5, beta=0.33, delta=0.33, gamma=0.33)
#print('done')

from sklearn import datasets
from random import random, randint

iris = datasets.load_iris()
X = np.asarray(iris.data)
# np.random.shuffle(X)
# X = X[0:20,]

user = np.asarray(iris.data[randint(0,149)])     # random user profile

budget = 3
bcm = BundleCmeans(c=3, user=user, budget=budget, sim=cos, eta = 10, m=0.8, epsilon=10e-5, max_nb_iter=100)
bcm.fit(X, alpha=0.5, beta=0.33, delta=0.33, gamma=0.33)
print('done')