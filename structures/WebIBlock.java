package dataStructures;

import java.util.ArrayList;

public class WebIBlock {

	
	
	private ArrayList<String> measures;
	private ArrayList<String> dimensions;
	private ArrayList<Filter> filters;
	private String idBlock;
	private String idReport;
	private String idDocument;
	private String universe;
	private String blocktype;
	private String blockviz;

	
	
	public WebIBlock() {
		this.measures = new ArrayList<String>();
		this.dimensions = new ArrayList<String>();
		this.filters = new ArrayList<Filter>();
		this.idBlock = "";
		this.idDocument = "";
		this.universe = "";
		this.blocktype="";
		this.blockviz="";
	}
	
	
	public WebIBlock(ArrayList<String> measures, ArrayList<String> dimnesions,
			ArrayList<Filter> filters, String idQuery, String idDocument,
			String universe, String blockviz, String blocktype, String idReport) {
		this.measures = measures;
		this.dimensions = dimnesions;
		this.filters = filters;
		this.idBlock = idQuery;
		this.idReport = idReport;
		this.idDocument = idDocument;
		this.universe = universe;
		this.blockviz = blockviz;
		this.blocktype = blocktype;
		
	}
	
	public ArrayList<String> getMeasures() {
		return measures;
	}
	public void setMeasures(ArrayList<String> measures) {
		this.measures = measures;
	}
	public ArrayList<String> getDimensions() {
		return dimensions;
	}
	public void setDimensions(ArrayList<String> dimensions) {
		this.dimensions = dimensions;
	}
	
	


	public ArrayList<Filter> getFilters() {
		return filters;
	}


	public void setFilters(ArrayList<Filter> filters) {
		this.filters = filters;
	}


	public String getIdBlock() {
		return idBlock;
	}
	public void setIdBlock(String idQuery) {
		this.idBlock = idQuery;
	}
	public String getIdDocument() {
		return idDocument;
	}
	public void setIdDocument(String idDocument) {
		this.idDocument = idDocument;
	}
	public String getUniverse() {
		return universe;
	}
	public void setUniverse(String universe) {
		this.universe = universe;
	}


	public String getBlocktype() {
		return blocktype;
	}


	public void setBlocktype(String blocktype) {
		this.blocktype = blocktype;
	}
	
	public String getBlockviz() {
		return blockviz;
	}


	public void setBlockviz(String blockviz) {
		this.blockviz = blockviz;
	}


	public String getIdReport() {
		return idReport;
	}


	public void setIdReport(String idReport) {
		this.idReport = idReport;
	}
	
	

}
