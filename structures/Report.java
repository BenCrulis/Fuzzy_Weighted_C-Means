package dataStructures;

import java.util.ArrayList;

public class Report {
	
	private String id;
	private String idDoc;
	private String idRep;
	private ArrayList<WebIBlock> blocks;
	
	
	
	
	
	public Report( String idDoc, String idRep,
			ArrayList<WebIBlock> blocks) {
		super();
		this.id = idDoc + '/' + idRep;
		this.idDoc = idDoc;
		this.idRep = idRep;
		this.blocks = blocks;
	}
	public String getId() {
		String id = this.idDoc + '/' + this.idRep;
		return id;
	}
	public void setId(String idDoc, String idRep) {
		this.id = idDoc + '/' + idRep;
	}
	public String getIdDoc() {
		return idDoc;
	}
	public void setIdDoc(String idDoc) {
		this.idDoc = idDoc;
	}
	public String getIdRep() {
		return idRep;
	}
	public void setIdRep(String idRep) {
		this.idRep = idRep;
	}
	public ArrayList<WebIBlock> getBlocks() {
		return blocks;
	}
	public void setBlocks(ArrayList<WebIBlock> blocks) {
		this.blocks = blocks;
	}
	
	

}
