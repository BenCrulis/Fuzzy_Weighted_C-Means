package calculations;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.lucene.analysis.util.CharArrayMap.EntrySet;

import dataStructures.Document;
import dataStructures.Filter;
import dataStructures.WebIBlock;

public class BlocFeatureBasedComparison {

	
	ArrayList<Document> contexts = new ArrayList<Document>();
	

	 private static String[] featureDimensions = {"universe", "measure", "dimension", "filter",  "viz", /*"user",*/ "folder", "report"};
	 private static String [] combBloc;
	 private static Double[] weights = {0.75444575 , 0.20940307,  0.05561886 ,-0.03982789 ,-0.03418615,  1.70225057,
		   1.81957678
};
	 private static final Map<String, String> vizTypes = new HashMap<String, String>();

	 public BlocFeatureBasedComparison ()
	{
		 // initialize vizTypes Map
		 vizTypes.put("100% Stacked Bar Chart", "ComparisonAmongItems");
		 vizTypes.put("Bar Chart", "ComparisonAmongItems");
		 vizTypes.put("3D Column Chart","ComparisonAmongItems");
		 vizTypes.put("Column Chart", "ComparisonAmongItems");
		 vizTypes.put("Column Chart With 2 Y-Axes", "ComparisonAmongItems");
		 vizTypes.put("Heat Map", "ComparisonAmongItems");
		 vizTypes.put("Horizontal Table", "ComparisonAmongItems");
		 vizTypes.put("Vertical Table", "ComparisonAmongItems");
		 vizTypes.put("Tag Cloud", "ComparisonAmongItems");
		 vizTypes.put("Area Chart", "ComparisonOverTime");
		 vizTypes.put("Polar Bubble Chart", "ComparisonOverTime");
		 vizTypes.put("Polar Scatter Plot", "ComparisonOverTime");
		 vizTypes.put("Combined Column Line Chart", "ComparisonOverTime");
		 vizTypes.put("Combined Column Line Chart With 2 Y-Axes", "ComparisonOverTime");
		 vizTypes.put("Line Chart", "ComparisonOverTime");
		 vizTypes.put("Line Chart With 2 Y-Axes", "ComparisonOverTime");
		 vizTypes.put("Radar Chart", "ComparisonOverTime");
		 vizTypes.put("Scatter Plot", "Distribution");
		 vizTypes.put("Angular Gauge", "Distribution");
		 vizTypes.put("Box Plot", "Distribution");
		 vizTypes.put("Geo Bubble Chart", "Distribution");
		 vizTypes.put("Geo Choropleth Chart", "Distribution");
		 vizTypes.put("Linear Gauge", "Distribution");
		 vizTypes.put("Speedometer", "Distribution");
		 vizTypes.put("Tile with deviation", "Distribution");
		 vizTypes.put("Tree Map", "Distribution");
		 vizTypes.put("100% Stacked Column Chart", "Mixture"); // Mixture for Composition
		 vizTypes.put("Waterfall Chart", "Mixture");
		 vizTypes.put("Donut Chart", "Mixture");
		 vizTypes.put("Geo Pie Chart", "Mixture");
		 vizTypes.put("Pie Chart", "Mixture");
		 vizTypes.put("Pie with Variable Slice Depth", "Mixture");
		 vizTypes.put("Stacked Column Chart", "Mixture");
		 vizTypes.put("Stacked Bar Chart", "Mixture");
		 vizTypes.put("Tile", "Mixture");
		 vizTypes.put("Bubble Chart", "Relationship");
		 vizTypes.put("Cross Table", "Relationship"); 
	}


	public static float[][] calculMatriceBlocs(WebIBlock[] allBlocs, String auditdatabase, String blockbase, Connection con) throws IOException, SQLException {

	    	int length = allBlocs.length;
	    	
	    
	    	HashMap<String,ArrayList<DocUsage>> usage = getTotalUsersOfDocument( auditdatabase,blockbase,con, "userid") ;
	    	HashMap<String,ArrayList<DocUsage>> folder = getTotalUsersOfDocument( auditdatabase,blockbase,con, "folderid") ;
	    	
	        int pos = 0; 
			int nbCombin = FeatureExtractionDocument.factorial(length);
			float [][]result = new float[nbCombin][featureDimensions.length+1];
			combBloc = new String[nbCombin];
			
	 		for (int i = 0; i < length - 1; i++) {
	 		//	System.out.println(System.currentTimeMillis());
				for (int j = i + 1; j < length; j++) {
					
					combBloc[pos] = "B"+(i+1)+"B"+(j+1);
					//combContext[pos] = "<"+contexts[i].getDocID()+"> - <"+contexts[j].getDocID()+">";
					for (int f = 0; f < featureDimensions.length; f++) {
						switch (featureDimensions[f]) {
						case "universe":
							result[pos][f] = sameUniverses(allBlocs[j], allBlocs[i]);
							break;
						case "measure" :
							result[pos][f] = (float) sameMeasOrDim(allBlocs[i],allBlocs[j], "Measure");
							break;
	// TODO: Change dimension and take into account the projected values in tables of graphs of the filtered dimensions
						case "dimension" :
							result[pos][f] = (float) sameMeasOrDim(allBlocs[i],allBlocs[j], "Dimension");
						break;
						case "filter" :
							result[pos][f] = (float) sameFilter(allBlocs[i],allBlocs[j]);
						break;
						case "user":
							result[pos][f] = sameUsers(allBlocs[i],allBlocs[j], auditdatabase, con, usage);
						break;
						case "viz" :
							result[pos][f] = (float) sameViz(allBlocs[i],allBlocs[j]);
						break;
						case "folder":
							result[pos][f] = sameTopFolders(allBlocs[i],allBlocs[j], auditdatabase, con, folder);
							break;
						case "report":
							result[pos][f] = sameReport(allBlocs[i],allBlocs[j], auditdatabase, con);
						break;
				//		case "docobject" :
				//			result[pos][f] = (float) sameObjects(contexts[i],contexts[j] );
				//		break;
				//		case "eventtype":
				//			result[pos][f] = sameEventType(contexts[i],contexts[j]);
				//		break;
						
						
				//		case "event":
				//			result[pos][f] = sameEvent(contexts[j], contexts[i]);
				//		break;
				//		case "session":
				//			result[pos][f] = sameSession(contexts[j], contexts[i]);
				//		break;
				//		case "period":
				//			result[pos][f] = samePeriod(contexts[j], contexts[i]);
				//		break;
				//		case "positionSession":
				//			result[pos][f] = samePositionInSession(contexts[j], contexts[i], position);
				//		break;
				//		case "popularity":
				//			result[pos][f] = samePopularity2(contexts[j], contexts[i], popularity);
				//		break;
				//		case "dimensions":
				//			result[pos][f] = (float) sameObjectsWithoutMeasures(contexts[j], contexts[i]);
				//		break;	
				//		case "titlekeywords" :
				//			result[pos][f] = (float) sameTitleKeywords(contexts[i],contexts[j], titles);
				//		break;
						default:
							System.out.println("No feature dimension !");
							
						}					
					}
		
					if(allBlocs[i].getIdDocument().equals(allBlocs[j].getIdDocument()))
						result[pos][featureDimensions.length] = (float) 1.0;
					else 
						result[pos][featureDimensions.length] = (float) -1.0;
					pos++;
					
					

							
				}
			//	System.out.println(System.currentTimeMillis());
			}
			return result;
		
	}

	
	


	/*** Returns 1 or 0 if the blocs are in the same report or not.
	 *   
	 *   @param b1 :  first bloc
	 *   @param b2 :  second bloc
	 *   
	 *   @return 1 or 0
	 * @throws SQLException 
	 * */
	private static float sameReport(WebIBlock webIBlock, WebIBlock webIBlock2,
			String auditdatabase, Connection con) {
		
		
		if(webIBlock.getIdDocument().equals(webIBlock2.getIdDocument()) && webIBlock.getIdReport().equals(webIBlock2.getIdReport()))
			return 1;
		
		else 
			return 0;
	}


	/*** Returns the score of similarity between two blocs
	 *   based on the users that have used the documents they are part of.
	 *   
	 *   @param b1 :  first bloc
	 *   @param b2 :  second bloc
	 *   
	 *   @return the score of dissimilarity between blocs 
	 *   based on the users that have used them.
	 * @throws SQLException 
	 * */
	private static float sameUsers(WebIBlock b1, WebIBlock b2, String auditdatabase, Connection con, HashMap<String, ArrayList<DocUsage>> usage ) throws SQLException {
		
			String doc1 = b1.getIdDocument();
			String doc2 = b2.getIdDocument();
			
			if(doc1.equals(doc2))
				return 1;
			
			
		
			/*** When retrieving each time the parents using the specific documents
			HashMap<String, Integer> users1 = getUsersOfDocument(doc1, auditdatabase, con);
			HashMap<String, Integer> users2 = getUsersOfDocument(doc2, auditdatabase, con);
			***/
		
			
			HashMap<String, Integer> users1 = new HashMap<String, Integer>();
			HashMap<String, Integer> users2 =new HashMap<String, Integer>();
			
			
			for(DocUsage du : usage.get(doc1))
				users1.put(du.getUserId(), du.getScore());

			for(DocUsage du : usage.get(doc2))
				users2.put(du.getUserId(), du.getScore());
			
			
			ArrayList<String> uniqueUsers = new ArrayList<String>();
			Iterator it = users1.entrySet().iterator();
			while(it.hasNext())
			{
				Map.Entry pair = (Entry) it.next();
				uniqueUsers.add((String) pair.getKey());
			}
			Iterator it2 = users2.entrySet().iterator();
			while(it2.hasNext())
			{
				Map.Entry pair = (Entry) it2.next();
				if(!uniqueUsers.contains((String)pair.getKey()))
				uniqueUsers.add((String) pair.getKey());
			}
		
				
				Integer[][] vectors = new Integer[2][uniqueUsers.size()];
				Arrays.fill(vectors[0], 0);
				Arrays.fill(vectors[1], 0);
				int index = 0;
				for (String measure : uniqueUsers){
					vectors[0][index] = users1.get(measure) != null ? users1.get(measure) : 0;
					vectors[1][index] = users2.get(measure) != null ? users2.get(measure) : 0;
					index++;
				}
			
			return (float) FeatureExtractionDocument.cosineQParts(vectors);
			
		}
	
	


	/** This feature checks if the blocks are related to the same universe.
	 * One bloc -> one universe
	 *  
	 *  @param b1 first bloc
	 *  @param b2 second bloc
	 *  @return 1 or 0 according the case, if they share the same universe or not
	 */  
	private static float sameUniverses(WebIBlock b1, WebIBlock b2) {
		
			if(b1.getUniverse().equals(b2.getUniverse()))
				return 1;
			else return 0;
	}
	
	
	/**This feature calculates the similarity between two blocs
	 *  in terms of the measures/dimensions used in documents.
	 * 
	 * @param b1  first bloc
	 * @param b2  second bloc
	 * @param bo  String that identifies Measures or Dimensions
	 * @return    the similarity between these blocs based on the 
	 * 			  measures/dimensions they contain 
	 
	 */
	public static float sameMeasOrDim(WebIBlock b1, WebIBlock b2, String bo  ) throws IOException {
		
		
		HashMap<String, Integer> bo1 = new HashMap<String, Integer>();
		HashMap<String, Integer> bo2 = new HashMap<String, Integer>();
		
		
		if(bo.equals("Measure"))
		{
		bo1 = FeatureExtractionDocument.getFrequences(bo1, b1.getMeasures());
		bo2 = FeatureExtractionDocument.getFrequences(bo2, b2.getMeasures());
		}
		else
		{
		bo1 = FeatureExtractionDocument.getFrequences(bo1, b1.getDimensions());
		bo2 = FeatureExtractionDocument.getFrequences(bo2, b2.getDimensions());
		}
		

        if(bo1.isEmpty() || bo2.isEmpty())
            return (float) 0.0;
		
		HashSet<String> bos = new HashSet<String>();
		
		bos.addAll(bo1.keySet());
		bos.addAll(bo2.keySet());		
		
		Integer[][] vectors = new Integer[2][bos.size()];
		Arrays.fill(vectors[0], 0);
		Arrays.fill(vectors[1], 0);
		
		
		float similarity;
		int index = 0;
		for (String b : bos){
			vectors[0][index] = bo1.get(b) != null ? bo1.get(b) : 0;
			vectors[1][index] = bo2.get(b) != null ? bo2.get(b) : 0;
			index++;
		}
		
		similarity = FeatureExtractionDocument.cosineQParts(vectors);
		
		float sim = (float) (similarity );
		return sim;
	}
	
	
	
	/**This feature calculates the similarity between two blocs
	 *  in terms of the filters applied on them (bloc and report filters),
	 *  comparing their values as well.
	 * 
	 * @param b1  first bloc
	 * @param b2  second bloc
	 * @return    the similarity between these blocs based on the 
	 * 			  filters they contain and their values 
	 
	 */
	static float sameFilter(WebIBlock b1, WebIBlock b2) {
		
		HashMap<String, Integer> filter1 = new HashMap<String, Integer>();
		HashMap<String, Integer> filter2 = new HashMap<String, Integer>();
		
		
		HashMap<String, ArrayList<String>> entries1 = new HashMap<String, ArrayList<String>>();
		for(Filter f : b1.getFilters())
			entries1.put(f.getFilterName(), (ArrayList<String>) f.getValues());
		HashMap<String, ArrayList<String>> entries2 = new HashMap<String, ArrayList<String>>();
		for(Filter f : b2.getFilters())
			entries2.put(f.getFilterName(), (ArrayList<String>) f.getValues());
		
		
				
		filter1 = FeatureExtractionDocument.getFilterFreq(filter1, entries1);
		filter2 = FeatureExtractionDocument.getFilterFreq(filter2, entries2);
		
		

        if(filter1.isEmpty() || filter2.isEmpty())
            return (float) 0.0;
		
        HashSet<String> filters = new HashSet<String>();
        
		filters.addAll(filter1.keySet());
		filters.addAll(filter2.keySet());		
		
		Integer[][] vectors = new Integer[2][filters.size()];
		Arrays.fill(vectors[0], 0);
		Arrays.fill(vectors[1], 0);
		
		
		float similarity;
		int index = 0;
		for (String b : filters){
			vectors[0][index] = filter1.get(b) != null ? filter1.get(b) : 0;
			vectors[1][index] = filter2.get(b) != null ? filter2.get(b) : 0;
			index++;
		}
		
		similarity = FeatureExtractionDocument.cosineQParts(vectors);
		
		float simVal = (float) 0.0;
		if(!b1.getFilters().isEmpty() && !b2.getFilters().isEmpty())
				simVal = sameFilterValues(b1, b2);
		
		float res = (float) (similarity*0.7 + simVal*0.3);
		return res;
	}
	
	private static float sameFilterValues(WebIBlock b1, WebIBlock b2) {
		
		float result=(float) 0.0;
		
		ArrayList<Filter> filters1 = b1.getFilters(), filters2 = b2.getFilters();
		
		if(filters1==null || filters2==null)
			return (float)0.0;
		if(!filters1.isEmpty() && !filters2.isEmpty())
		{
		for(Filter pair : filters1)
			{
			 for(Filter pair2: filters2)
				 {
				 ArrayList<String> simval = FeatureExtractionDocument.getSimilarValues((ArrayList<String>) pair.getValues(), (ArrayList<String>) pair2.getValues());
				 if(pair.getFilterName().equals(pair2.getFilterName()) && !simval.isEmpty())
				 {
					     
				 	
				 		if(pair.getValues().get(0).equals(pair2.getValues().get(0)) // if they both use the same operator
				 				|| (pair.getValues().get(0).contains("GREATER") && pair2.getValues().get(0).contains("GREATER"))
				 					|| (pair.getValues().get(0).contains("LESS") && pair2.getValues().get(0).contains("LESS"))) // GREATER/LESS same with GREATER/LESS OREQUAL
				 		{
				 			if(pair.getValues().get(0).contains("EQUAL") || pair.getValues().get(0).equals("GREATER") || pair.getValues().get(0).equals("LESS") || pair.getValues().get(0).contains("NULL")) // includes NOTEQUAL, GREATEROREQUAL, LESSOREQUAL, NOTNULL
				 				result++;
				 			else if (pair.getValues().get(0).contains("INLIST")) // includes NOTINLIST
				 				result += (float) simval.size() / (float) Math.max(pair.getValues().size()-1, pair2.getValues().size()-1);
				 			else if (pair.getValues().get(0).equals("BETWEEN"))
				 			{
				 				if(simval.size()==2)	
				 					result++;
				 				else {
				 					float diff = (float) 0.0;
				 					if(pair.getFilterName().contains("eek"))
				 					{
				 						String week11 = pair.getValues().get(3).substring(pair.getValues().get(3).indexOf("-")+2,pair.getValues().get(3).length());
				 						String week21 = pair2.getValues().get(3).substring(pair2.getValues().get(3).indexOf("-")+2,pair2.getValues().get(3).length());
				 						String week12 = pair.getValues().get(6).substring(pair.getValues().get(6).indexOf("-")+2,pair.getValues().get(6).length());
				 						String week22 = pair2.getValues().get(6).substring(pair2.getValues().get(6).indexOf("-")+2,pair2.getValues().get(6).length());
				 							diff = Math.max(Math.abs(Integer.valueOf(week11)-Integer.valueOf(week21)) 
				 									, Math.abs(Integer.valueOf(week12)-Integer.valueOf(week22))); // difference between different values of BETWEEN operator
				 							
				 							result += diff / (float) Math.max(Math.abs(Integer.valueOf(week11)-Integer.valueOf(week12)), Math.abs(Integer.valueOf(week21))-Integer.valueOf(week22)); // max range of BETWEEN
				 					}
				 						else
				 						{
				 						diff = Math.max(Math.abs(Integer.valueOf(pair.getValues().get(1))-Integer.valueOf(pair2.getValues().get(1))), Math.abs(Integer.valueOf(pair.getValues().get(2))-Integer.valueOf(pair2.getValues().get(2)))); // difference between different values of BETWEEN operator
				 						result += diff / (float) Math.max(Math.abs(Integer.valueOf(pair.getValues().get(1))-Integer.valueOf(pair.getValues().get(2))), Math.abs(Integer.valueOf(pair2.getValues().get(1))-Integer.valueOf(pair2.getValues().get(2)))); // max range of BETWEEN
				 						}
				 				}
				 					
				 					
				 			}
				 		}
				 		else // if the operators are different, there are some cases to consider that interrogates close parts of the database
				 		{
				 		
				 		if((pair.getValues().get(0).equals("EQUAL") || pair.getValues().get(0).equals("INLIST")) 
				 			&& (pair2.getValues().get(0).equals("EQUAL") || pair2.getValues().get(0).equals("INLIST")))
				 			result += (float) simval.size() / (float) Math.max(pair.getValues().size()-1, pair2.getValues().size()-1);
				 		else if ((pair.getValues().get(0).equals("EQUAL") || pair.getValues().get(0).equals("GREATEROREQUAL") || pair.getValues().get(0).equals("LESSOREQUAL")) 
					 			&& (pair2.getValues().get(0).equals("EQUAL") || pair.getValues().get(0).equals("GREATEROREQUAL") || pair.getValues().get(0).equals("LESSOREQUAL")))
				 			result += 0.1;	// static value to test, to simplify the calculations and not to calculate all the possible values according to filters
				 		else if ((pair.getValues().get(0).equals("EQUAL") || pair.getValues().get(0).equals("BETWEEN"))
				 				&& (pair2.getValues().get(0).equals("EQUAL") || pair2.getValues().get(0).equals("BETWEEN")))
				 		{
				 			if(pair.getValues().size()<3) // BETWEEN is the operator of the second filter
				 			result += (float) simval.size() / (float) Math.abs(Integer.valueOf(pair2.getValues().get(1))-Integer.valueOf(pair2.getValues().get(2)));
				 			else // BETWEEN is the operator of the first filter
				 			{
				 				if(pair.getValues().get(1).matches("[0-9]+") && pair.getValues().get(2).matches("[0-9]+"))
				 				result += (float) simval.size() / (float) Math.abs(Integer.valueOf(pair.getValues().get(1))-Integer.valueOf(pair.getValues().get(2)));
				 			}
				 		}
				 		else if ((pair.getValues().get(0).equals("INLIST") || pair.getValues().get(0).contains("GREATER") || pair.getValues().get(0).contains("LESS") )   // includes GREATEROREQUAL & LESSOREQUAL
				 				&& (pair2.getValues().get(0).equals("INLIST") || pair2.getValues().get(0).contains("GREATER") || pair2.getValues().get(0).contains("LESS")))
				 		{
				 			int pos ;
				 				if(pair.getValues().size()>1) // INLIST is the operator of the first filter
				 				{
				 					Collections.sort(pair.getValues()); // order the values of the array
				 					pos = pair.getValues().indexOf(pair2.getValues().get(1)); // get the position of the common value 
				 					if(pair2.getValues().get(0).contains("OREQUAL"))
				 					result += (float) (pos+1) / (float) (pair.getValues().size());
				 					else 
				 						result += (float) pos / (float) (pair.getValues().size());
				 				}
					 			else // INLIST is the operator of the second filter
					 			{
					 				Collections.sort(pair2.getValues()); // order the values of the array
				 					pos = pair2.getValues().indexOf(pair.getValues().get(1)); // get the position of the common value 
				 					if(pair.getValues().get(0).contains("OREQUAL"))
				 					result += (float) (pos+1) / (float) (pair2.getValues().size());
				 					else 
				 						result += (float) pos / (float) (pair2.getValues().size());
					 			}
				 		}
				 		else if ((pair.getValues().get(0).equals("INLIST") || pair.getValues().get(0).equals("BETWEEN"))
				 				&& (pair2.getValues().get(0).equals("INLIST") || pair2.getValues().get(0).equals("BETWEEN")))
				 		{
				 			int dist1, dist2;
				 			if(pair.getValues().size()>1) // INLIST is the operator of the first filter
			 				{
			 					Collections.sort(pair.getValues()); // order the values of the array
			 					dist1 = (Integer.valueOf(pair2.getValues().get(1))-Integer.valueOf(pair.getValues().get(1))>0) ? (Integer.valueOf(pair2.getValues().get(1))-Integer.valueOf(pair.getValues().get(1))) : 0;
			 					dist2 = (Integer.valueOf(pair2.getValues().get(1))-Integer.valueOf(pair.getValues().get(1))>0) ? (Integer.valueOf(pair2.getValues().get(1))-Integer.valueOf(pair.getValues().get(1))) : 0;
			 					result += 1-((float) (dist1+dist2) / (float) (pair.getValues().size()));
			 				}
				 			else // INLIST is the operator of the second filter
				 			{
				 				Collections.sort(pair2.getValues()); // order the values of the array
				 				dist1 = (Integer.valueOf(pair.getValues().get(1))-Integer.valueOf(pair2.getValues().get(1))>0) ? (Integer.valueOf(pair.getValues().get(1))-Integer.valueOf(pair2.getValues().get(1))) : 0;
			 					dist2 = (Integer.valueOf(pair.getValues().get(1))-Integer.valueOf(pair2.getValues().get(1))>0) ? (Integer.valueOf(pair.getValues().get(1))-Integer.valueOf(pair2.getValues().get(1))) : 0;
			 					result += 1-((float) (dist1+dist2) / (float) (pair2.getValues().size()));
				 			}
				 		}
				 		else if ((pair.getValues().get(0).equals("BETWEEN") || pair.getValues().get(0).contains("GREATER"))
				 				&& (pair2.getValues().get(0).equals("BETWEEN") || pair2.getValues().get(0).contains("GREATER")))
				 		{
				 			if(pair.getValues().size()>1) // BETWEEN is the operator of the first filter
			 				{
				 				if(Integer.valueOf(pair2.getValues().get(1))<Integer.valueOf(pair.getValues().get(1))) 
				 					result +=1;
				 				else if (Integer.valueOf(pair2.getValues().get(1))<Integer.valueOf(pair.getValues().get(2)))
				 					result += Integer.valueOf(pair.getValues().get(2))-Integer.valueOf(pair2.getValues().get(1)) 
				 								/ (float)(Integer.valueOf(Integer.valueOf(pair.getValues().get(2))-Integer.valueOf(Integer.valueOf(pair.getValues().get(1)))));
			 				}
				 			else // BETWEEN is the operator of the second filter
				 			{
				 				if(Integer.valueOf(pair.getValues().get(1))<Integer.valueOf(pair2.getValues().get(1))) 
				 					result +=1;
				 				else if (Integer.valueOf(pair.getValues().get(1))<Integer.valueOf(pair2.getValues().get(2)))
				 					result += Integer.valueOf(pair2.getValues().get(2))-Integer.valueOf(pair.getValues().get(1)) 
				 								/ (float)(Integer.valueOf(Integer.valueOf(pair2.getValues().get(2))-Integer.valueOf(Integer.valueOf(pair2.getValues().get(1)))));
				 			}				 		
				 			}
				 		else if ((pair.getValues().get(0).equals("BETWEEN") || pair.getValues().get(0).contains("LESS"))
				 				&& (pair2.getValues().get(0).equals("BETWEEN") || pair2.getValues().get(0).contains("LESS")))
				 		{
				 			if(pair.getValues().size()>1) // BETWEEN is the operator of the first filter
			 				{
				 				if(Integer.valueOf(pair2.getValues().get(1))>Integer.valueOf(pair.getValues().get(2))) 
				 					result +=1;
				 				else if (Integer.valueOf(pair2.getValues().get(1))>Integer.valueOf(pair.getValues().get(1)))
				 					result += Integer.valueOf(pair2.getValues().get(1))-Integer.valueOf(pair.getValues().get(1)) 
				 								/ (float)(Integer.valueOf(Integer.valueOf(pair.getValues().get(2))-Integer.valueOf(Integer.valueOf(pair.getValues().get(1)))));
			 				}
				 			else // BETWEEN is the operator of the second filter
				 			{
				 				if(Integer.valueOf(pair.getValues().get(1))>Integer.valueOf(pair2.getValues().get(2))) 
				 					result +=1;
				 				else if (Integer.valueOf(pair.getValues().get(1))>Integer.valueOf(pair2.getValues().get(1)))
				 					result += Integer.valueOf(pair.getValues().get(1))-Integer.valueOf(pair2.getValues().get(1)) 
				 								/ (float)(Integer.valueOf(Integer.valueOf(pair2.getValues().get(2))-Integer.valueOf(Integer.valueOf(pair2.getValues().get(1)))));
				 			}				 		
				 			}				 			
				 		
				 	 
				 		}
				 			
				 }
				 }
			
			}
			}
		
	return result/(float) Math.max(filters1.size(), filters2.size());
}

	
	/***
	 * 
	 * @param doc1             first parameter is the document
	 * @param auditdatabase	   audit database which contains the usage of document from users
	 * @param con			   connection to the database
	 * @return				   Return the users and their frequence for the doc1
	 * @throws SQLException 
	 */
	private static HashMap<String, Integer> getUsersOfDocument(String doc1, String auditdatabase, Connection con) throws SQLException {

		HashMap<String, Integer> result = new HashMap<String, Integer>();
		Statement smt = con.createStatement();
		String sql = "Select userid from "+auditdatabase+" where parent='"+doc1+"'";
		ResultSet rs = smt.executeQuery(sql);
		
		while(rs.next())
		{
			if(result.containsKey(rs.getString(1)))
			{
				int val = result.get(rs.getString(1));
				result.replace(rs.getString(1), ++val);
			}
			else
				result.put(rs.getString(1), 1);
		}
		
		return result;
	}
	
	/*** Charge in memory all the parents and the frequency of usage for each document.
	 * This to reduce time of interrogating each time the database.
	 */
/*	private static Integer[][] getTotalUsersOfDocument( String auditdatabase,String blockbase, Connection con, ArrayList<String> docs, ArrayList<String> users) throws SQLException {

		
		Statement smt = con.createStatement();
		String sql = "Select distinct parent from "+blockbase+"";
		ResultSet rs = smt.executeQuery(sql);
		while(rs.next())
		{
			docs.add(rs.getString(1));
		}
		
		rs.close();
	
		sql = "Select distinct userid from "+auditdatabase+" where parent IN (Select distinct parent from "+blockbase+") order by parent";
		rs = smt.executeQuery(sql);
		while(rs.next())
		{
			users.add(rs.getString(1));
		}
		
		rs.close();
		
		Integer[][] result = new Integer[docs.size()][users.size()];
		
		sql = "Select parent, userid from "+auditdatabase+" where parent IN (Select distinct parent from "+blockbase+") order by parent";
		rs = smt.executeQuery(sql);
		
		for(String doc: docs)
		{
			int posDoc = docs.indexOf((String) doc);
			HashMap<String, Integer> usage = getUsersOfDocument(doc, auditdatabase, con);
			Iterator it = usage.entrySet().iterator();
			while(it.hasNext())
			{
				Map.Entry pair = (Entry) it.next();
				int posUser = users.indexOf((String) pair.getKey());
				result[posDoc][posUser] = (Integer) pair.getValue();
			}
		}
		
		
		return result;
	}
		*/
	
	
	static HashMap<String, ArrayList<DocUsage>> getTotalUsersOfDocument( String auditdatabase,String blockbase, Connection con, String object) throws SQLException {

		
		HashMap<String, ArrayList<DocUsage>>  result =  new HashMap<String, ArrayList<DocUsage>>(); 
		Statement smt = con.createStatement();
	//    String	sql = "Select parent, "+object+" from "+auditdatabase+" where parent IN (Select distinct parent from "+blockbase+") order by parent, "+object+"";
		String	sql = "Select parent, "+object+" from "+auditdatabase+" order by parent, "+object+"";
		ResultSet rs = smt.executeQuery(sql);
		String parent = "";
		String user="";
		int score=0;
		
		DocUsage du = new DocUsage();

		ArrayList<DocUsage> listdu = new ArrayList<DocUsage>();
		while(rs.next())
		{
			if(!parent.equals(rs.getString(1)))
			{
				
				if(!parent.equals(""))
				{
					du.setScore(score);
					listdu.add(du);
					result.put(parent, listdu);
					listdu = new ArrayList<DocUsage>();
					score = 0;
					user = rs.getString(2);
					du = new DocUsage(rs.getString(1), rs.getString(2), -1);
				}
				parent = rs.getString(1);
			}
			
				if(parent.equals(rs.getString(1)) && !user.equals(rs.getString(2)))
				{
					
					if (!user.equals(""))
					{
						du.setScore(score);
						listdu.add(du);
					}
					user = rs.getString(2);
					du = new DocUsage();
				
				du = new DocUsage(rs.getString(1), rs.getString(2), -1);
				score = 0;
				}
				
			
			score++;
		}
			
		return result;
			
	}

	
	/***
	 * 
	 * @param doc1             first parameter is the document
	 * @param auditdatabase	   audit database which contains the usage of document from users
	 * @param con			   connection to the database
	 * @return				   Return the folders where the documents are found
	 * @throws SQLException 
	 */
	private static ArrayList<String> getFoldersForDocument(String doc1, String auditdatabase, Connection con) throws SQLException {

		ArrayList<String> result = new ArrayList<String>();
		Statement smt = con.createStatement();
		String sql = "Select distinct folderid from "+auditdatabase+" where parent='"+doc1+"'";
		ResultSet rs = smt.executeQuery(sql);
		
		while(rs.next())
		{
			result.add(rs.getString(1));
		}
		
		return result;
	}


	
	// TODO:  the value returned for similarities between viz classes maybe to be changed!
/**
 * 
 * @param webIBlock    first bloc
 * @param webIBlock2   second bloc
 * @return   1 if the blocs' visualization is the same,
 *           ]0,1[ if the viz is not the same but they present both 
 *           		the same type of analysis,
 *           0 otherwise
 */
	private static float sameViz(WebIBlock webIBlock, WebIBlock webIBlock2) {

		float result = (float) 0.0;
		if(webIBlock.getBlockviz()!= null && webIBlock2.getBlockviz()!=null)
		{
		if(webIBlock.getBlockviz().equals(webIBlock2.getBlockviz()))
			return 1;
		else
		{
			String viz1class = vizTypes.get(webIBlock.getBlockviz());
			String viz2class = vizTypes.get(webIBlock2.getBlockviz());
			
			if(viz1class!=null && viz2class!=null)
			{
			if(viz1class.equals(viz2class))
				return (float) 0.5;  
			else if(viz1class.startsWith("C") && viz2class.startsWith("C"))
				return (float) 0.3;
			
			}
		}
		}
		
		return 0;
	}
	
	
	/** This feature calculates the similarity between two blocs
	 *  in terms of the folders the documents they are part of are classified into.
	 *  
	 *  @param b1 first bloc
	 *  @param b2 second bloc
	 *  @return similarity score between the folders these documents
	 *  	    are classified into. In the case of only two documents,
	 *  		belonging each to one folder, it will be a boolean score.
	 * @throws SQLException 
	 */
	private static float sameTopFolders(WebIBlock b1, WebIBlock b2, String auditdatabase, Connection con, HashMap<String, ArrayList<DocUsage>> usage) throws SQLException {

		String doc1 = b1.getIdDocument();
		String doc2 = b2.getIdDocument();
		
		if(doc1.equals(doc2))
			return 1;
		
	//	ArrayList<String> f1 = getFoldersForDocument(doc1, auditdatabase, con);
	//	ArrayList<String> f2 = getFoldersForDocument(doc2, auditdatabase, con);
		
		ArrayList<String> f1 = new ArrayList<String>();
		ArrayList<String> f2 = new ArrayList<String>();
		
		
		for(DocUsage du : usage.get(doc1))
			f1.add(du.getUserId());

		for(DocUsage du : usage.get(doc2))
			f2.add(du.getUserId());
		
		if(f1.size()>1 || f2.size()>1)
			System.out.print("");
		
		return (float) FeatureExtractionDocument.maxFraction(f1, f2);
	
	}
	
	
	/**
	 * Print the matrix of dissimilarity calculated by {@code calculMatriceSelectedDocs or calculateMatrice}
	 * @param fileName : the file where the matrix will be written
	 * @param matrix   : the matrix of dissimilarity
	 */
	public static void writeCsvFile(String fileName ,float[][] matrix) {
		
		 String COMMA_DELIMITER = ";";
		 String NEW_LINE_SEPARATOR = "\n";
		 String FILE_HEADER = "contexts";
		 int length = matrix.length;
		
		 for (int i = 0; i< featureDimensions.length; i++)
		 {
			 FILE_HEADER = FILE_HEADER+";"+featureDimensions[i].toString();
		 }
		 FILE_HEADER = FILE_HEADER+"; category";
		 System.out.println(FILE_HEADER);
		 try {
			        FileWriter fileWriter = new FileWriter(fileName);
			        fileWriter.append(FILE_HEADER.toString());
			        fileWriter.append(NEW_LINE_SEPARATOR);
			        for (int i = 0; i<length; i++ )
			        {
			        	fileWriter.append(combBloc[i]);
			        	fileWriter.append(COMMA_DELIMITER);
			        	for (int j=0; j<featureDimensions.length+1; j++)
			        	{
			        		fileWriter.append(String.valueOf(matrix[i][j]));
			        		if(j!=featureDimensions.length)
			        		fileWriter.append(COMMA_DELIMITER);
			        	}
			        	fileWriter.append(NEW_LINE_SEPARATOR);
			        }
			                 
			         fileWriter.flush();
	                 fileWriter.close();
			
            System.out.println("CSV file was created successfully !!!");
			         } catch (Exception e) {
			             System.out.println("Error in CsvFileWriter !!!");
			            e.printStackTrace();
			         } finally {
			           
			         }
	}
	
	

	static double compareBlocs(WebIBlock wb1, WebIBlock wb2, String auditbase,String blockbase, Connection con, HashMap<String, ArrayList<DocUsage>> usage, HashMap<String, ArrayList<DocUsage>> folder) throws SQLException, IOException {
		
		double weights [] = {  0.75444575 , 0.20940307,  0.05561886 ,-0.03982789 ,-0.03418615,  1.70225057,
				   1.81957678
};

    	
		double sum = 0.0;
		for (int f = 0; f < featureDimensions.length; f++) 
		{
			switch (featureDimensions[f]) {				
			case "universe":
				sum+= sameUniverses(wb1,wb2)*weights[0];
				break;
			case "measure" :
				sum+= (float) sameMeasOrDim(wb1,wb2, "Measure")*weights[1];
				break;
			case "dimension" :
				sum+= (float) sameMeasOrDim(wb1,wb2, "Dimension")*weights[2];
			break;
			case "filter" :
				sum+=  (float) sameFilter(wb1,wb2)*weights[3];
			break;
			case "viz" :
				sum+= (float) sameViz(wb1, wb2)*weights[4];
			break;
			case "user":
				sum+= sameUsers(wb1,wb2, auditbase, con, usage)*weights[5];
			break;
			case "folder":
				sum+= sameTopFolders(wb1, wb2, auditbase, con, folder)*weights[5];
				break;
			case "report":
				sum+= sameReport(wb1, wb2, auditbase, con)*weights[6];
			break;
			default:
				System.out.println("No feature dimension !");
		}	
		}
		
		
	
		return (1-(double)sum/(double)featureDimensions.length);//
	
		
	}
}

	

