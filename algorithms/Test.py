from sklearn import datasets
from sklearn.metrics.cluster import adjusted_rand_score
from sklearn.cluster import KMeans

import numpy as np
from algorithms.KM import KM
from algorithms.FCM import FCM
from algorithms.FCMcos import FCMcos

#iris = datasets.load_iris()
d = datasets.load_breast_cancer()
X = d.data
y = d.target



#X = np.asarray([[3,2,2], [4,1,2], [0,0,8], [0,1,7], [1,1,7]])

# Use K-means clustering
"""
kmeans = KM(2)  # build model
kmeans.fit(X)   # fit model to the dataset
print("My k-means: %f" %(adjusted_rand_score(kmeans.getClusters(),y)))

kms = KMeans(init='random', n_clusters=2, n_init=1)
kms.fit(X)
print("Their k-means: %f" %(adjusted_rand_score(kms.labels_,y)))
"""
fcmeans = FCM(2)
fcmeans.fit(X)
print("My fc-means: %f" %(adjusted_rand_score(fcmeans.getClusters(),y)))
"""
fcmc = FCMcos(2)
fcmc.fit(X)
print("My fc-means (cosine sim): %f" %(adjusted_rand_score(fcmc.getClusters(),y)))
"""

path = "/home/alex/stage/"

iris = np.genfromtxt(path + 'iris.csv', delimiter=';')
#X = iris[:,:4]
#Y = iris[:,-1]
# print("done")